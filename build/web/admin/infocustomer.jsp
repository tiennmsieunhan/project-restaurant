<%-- 
    Document   : infocus
    Created on : Oct 26, 2019, 6:20:15 PM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Information Customer</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="../css/both.css" rel="stylesheet" type="text/css"/>

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>
            td.indent{padding-left: 15.5em}
        </style>
    </head>
    <body>
        <jsp:include page="../admin/headeradmin.jsp"></jsp:include>
            <form action="../incus" method="post">
                <!--Section title-->
                <div class="container">
                    <div class="row py-5">
                        <div class="col text-center">
                            <h3 class="text-dark" style="font-weight: bold">
                                Information of Customer
                            </h3>
                            <div class="title-underlind mx-auto"></div>
                        </div>   
                    </div>
                </div>
                <table class="single-item d-flex justify-content-between my-3 p-3 ">
                    <tr class="single-item-text">
                        <td class="text-uppercase text-dark " style=" font-weight: bold">Your Name:</td>
                        <td style="padding-left: 4.2em">
                            <div class="col-sm-10">
                                <div class="form-group p-2">
                                    <label for="exampleInputName2"></label>
                                    <input name="nameBook" type="text" class="form-control"
                                           id="exampleInputName2" placeholder="Your Name..." required size="70" value="${namecus}" disabled>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="single-item-text">
                    <td class="text-uppercase text-dark " style=" font-weight: bold">Phone: </td>
                    <td style="padding-left: 4.2em">
                        <div class="col-sm-10">
                            <div class="form-group p-2">
                                <label for="exampleInputName2"></label>
                                <input name="phoneBook" type="text" class="form-control"
                                       id="exampleInputName2" placeholder="Your Phone..." required value="${phonecus}" disabled>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="single-item-text">
                    <td class="text-uppercase text-dark " style=" font-weight: bold">Email: </td>
                    <td style="padding-left: 4.2em">
                        <div class="col-sm-10">
                            <div class="form-group p-2">
                                <label for="exampleInputName2"></label>
                                <input name="emailBook" type="text" class="form-control"
                                       id="exampleInputName2" placeholder="Your Email..." required value="${emailcus}" disabled>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="single-item-text">
                    <td class="text-uppercase text-dark " style=" font-weight: bold">Note: </td>
                    <td style="padding-left: 4.2em">
                        <div class="col-sm-10">
                            <div class="form-group p-2">
                                <label for="exampleInputName2"></label>
                                <textarea name="noteBook" type="text" class="form-control"
                                          id="exampleInputName2" required value="${notecus}" disabled ></textarea>
                            </div>
                        </div>
                    </td>
                </tr>

            </table>
        </form>
        <jsp:include page="../admin/footeradmin.jsp"></jsp:include>             
    </body>
</html>
