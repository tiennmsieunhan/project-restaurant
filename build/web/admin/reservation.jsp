<%-- 
    Document   : header
    Created on : Sep 21, 2019, 5:40:44 PM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Management Booking Table</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="../css/both.css" rel="stylesheet" type="text/css"/>

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>
            td.indent{padding-left: 12.5em}
            tr.indent{padding-bottom:  20.5em}
        </style>
    </head>
    <body>
        <jsp:include page="../admin/headeradmin.jsp"></jsp:include>
            <form id="choisedate" name="choisedate"> 
            <jsp:useBean id="show" class="dao.ShowTableBooked" scope="session"></jsp:useBean>
                <div class="container">
                    <div class="row py-5">
                        <div class="col text-center" >
                            <p class="display-4 text-uppercase text-light mb-0" style="font-size: 37px">
                                <strong class="text-dark">
                                    Management Reservation of Sea Dog
                                </strong>
                            </p>
                            <div class="title-underlind mx-auto"></div>
                            <table class="single-item d-flex justify-content-between my-3 p-3" >
                                <tr class="indent"></tr>
                                <tr class="single-item-text ">
                                    <td class=" text-uppercase text-dark " >Need to know&emsp;</td>
                                    <td class="indent"></td>
                                    <td><p style="color: #53131e; font-size: 20px;">You just can watch information of Guests have booked tables in year   </p></td>
                                </tr>
                                <tr class="indent"></tr>
                            </table>
                            <table class="single-item d-flex justify-content-between my-3 p-3 ">
                                <tr class="single-item-text">
                                    <td class="indent"></td>
                                    <td class="text-uppercase text-dark " style=" font-weight: bold">Select Date:</td>
                                    <td style="padding-left: 4.2em">
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <button type="button" class="btn btn-outline-secondary docs-datepicker-trigger" disabled>
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </button>
                                                </div>
                                                <input class="form-control" id="date1" name="date" placeholder="DD/MM/YYYY" onchange="load()" type="text" value="${param.date}" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="https://www.facebook.com/easybistro/" >
                                        <img src="../img/facebook.png" width="50px" height="50px">
                                    </a>
                                </td>
                                <td>
                                    <a href="https://twitter.com/easybistro?lang=en" target="_blank">
                                        <img src="../img/twitter.png" width="50px" height="50px">
                                    </a>
                                </td>
                                <td>
                                    <a href="https://www.instagram.com/easybistro/" target="_blank">
                                        <img src="../img/instagram.png" width="50px" height="50px">
                                    </a>
                                </td>
                            </tr>
                        </table>

                    </div>   
                </div>
            </div>
            <jsp:setProperty name="show" property="day" value="${param.date}"></jsp:setProperty>

                <!--Table button-->
                <div class="container">
                    <div class="row py-5">
                        <div class="col text-center">

                        <c:if test="${empty show.listshow}">
                            <p style="color: #53131e; font-size: 20px; font-weight: bold">No visitors yet book table</p>
                        </c:if>

                        <c:if test="${not empty show.listshow}">

                            <table class="edit" border="1"  cellspacings="0" align="center">
                                <tr>
                                    <th></th>
                                    <th>Time</th>
                                    <th>Place</th>
                                    <th>Number Guest</th>
                                    <th>Date</th>
                                    <th>Customer ID</th>
                                </tr>
                                <c:forEach var="i" items="${show.listshow}">
                                    <tr>
                                        <jsp:setProperty name="show" property="tabid" value="${i.tableid}"></jsp:setProperty>
                                        <td>${show.namemeal}</td>
                                        <td>${show.timemeal}</td>
                                        <td>${show.nametype}</td>
                                        <td>${i.guest}</td>
                                        <td>${i.day}</td>
                                        <td>
                                            <a href="../incus?submitCus=${i.customerid}">
                                                <button type="button" class="btn btn-event-Modal" style="background-color: #53131e; border-color: #ffff00; color: #ffcccc;">
                                                    Information Customer
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>

                        </c:if>

                    </div>
                </div>
            </div>

        </form>



        <!-- Include jQuery -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

        <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        var date_input = $('input[name="date"]'); //our date input has the name "date"
                                                        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                                                        date_input.datepicker({
                                                            format: 'dd/mm/yyyy',
                                                            container: container,
                                                            todayHighlight: true,
                                                            autoclose: true,
                                                            startDate: '-6m',
                                                            endDate: '+6m'

                                                        });

                                                    })

        </script>
    </body>
</html>
<script>
    function load()
    {
        document.choisedate.submit()
    }
</script>

