<%-- 
    Document   : booktable
    Created on : Oct 24, 2019, 10:37:17 PM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Table Booking</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="css/both.css" rel="stylesheet" type="text/css"/>

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>td.indent{padding-left: 40.5em}</style>
    </head>
    <body>
        <jsp:include page="headerbook.jsp"></jsp:include>
            <form action="booked" method="post" >
                <div class="container">
                    <div class="row py-5">
                        <div class="col text-center" >
                            <h5 class="display-4 text-uppercase text-light mb-0">
                                <strong class="text-dark">
                                    Sea Dog
                                </strong>
                            </h5>
                            <div class="title-underlind mx-auto"></div>

                            <table class="single-item d-flex justify-content-between my-3 p-3" >
                                <tr class="single-item-text ">
                                    <td class=" text-uppercase text-dark " >DETAILS:&emsp;</td>
                                    <td>
                                        <p style="color: #53131e">
                                        <ul style="font-size: 30px; color: #cc0033"> <img src="img/calendar.png" width="30px" height="30px">&emsp;${cal}</ul>
                                    <ul style="font-size: 30px; color: #cc0033"> <img src="img/guest.png" width="30px" height="30px">&emsp; ${infoguest} </ul>
                                </td>
                            </tr>
                            <tr class="single-item-text ">
                                <td class=" text-uppercase text-dark " style="font-weight: bold" >MENU HIGHLIGHTS: &emsp;</td>
                                <td>    
                                    <ul style="color: #53131e">
                                        <li>  Fresh Oysters Daily</li>
                                        <li>Charcuterie & Cheese Board</li>
                                        <li>Steak Frites (wagyu sirloin, her butter, house cut fries)</li>
                                        <li>Bourbon bread pudding (caramel, bourbon cream, candied pecans)</li>
                                        <li> Our Menu is printed every day with a rotating selection of seasonal produce and fresh seafood</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="single-item-text ">
                                <td class=" text-uppercase text-dark " style="font-weight: bold" >CANCELLATION POLICY: &emsp;</td>
                                <td>    
                                    <ul style="color: #53131e">
                                        While you won't be charged if you need to cancel, we ask that you do so at least 24 hours in advance.
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <table class="single-item d-flex justify-content-between my-3 p-3 ">
                            <tr class="single-item-text">
                                <td class="text-uppercase text-dark " style=" font-weight: bold">Your Name:</td>
                                <td style="padding-left: 4.2em">
                                    <div class="col-sm-10">
                                        <div class="form-group p-2">
                                            <label for="exampleInputName2"></label>
                                            <input name="nameBook" type="text" class="form-control"
                                                   id="exampleInputName2" placeholder="Your Name..." required size="70" value="${acc.name}">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="single-item-text">
                                <td class="text-uppercase text-dark " style=" font-weight: bold">Phone: </td>
                                <td style="padding-left: 4.2em">
                                    <div class="col-sm-10">
                                        <div class="form-group p-2">
                                            <label for="exampleInputName2"></label>
                                            <input name="phoneBook" type="text" class="form-control"
                                                   id="exampleInputName2" placeholder="Your Phone..." required value="${acc.phone}">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="single-item-text">
                                <td class="text-uppercase text-dark " style=" font-weight: bold">Email: </td>
                                <td style="padding-left: 4.2em">
                                    <div class="col-sm-10">
                                        <div class="form-group p-2">
                                            <label for="exampleInputName2"></label>
                                            <input name="emailBook" type="text" class="form-control"
                                                   id="exampleInputName2" placeholder="Your Email..." required value="${acc.email}">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="single-item-text">
                                <td class="text-uppercase text-dark " style=" font-weight: bold">Note: </td>
                                <td style="padding-left: 4.2em">
                                    <div class="col-sm-10">
                                        <div class="form-group p-2">
                                            <label for="exampleInputName2"></label>
                                            <textarea name="noteBook" type="text" class="form-control"
                                                      id="exampleInputName2"   ></textarea>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </table>
                        <table>
                            <tr class="single-item-text">

                                <td class="indent">
                                    <input type="hidden" name="booking" value="${send}"/>
                                </td>
                                <td>
                                    <!--button Notify Me-->
                                    <div class="input-group-addon">                                       
                                        <button type="submit" class="btn btn-drink-Modal" name="booking" style="font-size: 24px" onclick="return confirm('Booking Successfully!!!')">
                                            Booking</button>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>   
                </div>
            </div>     

        </form>
        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>
