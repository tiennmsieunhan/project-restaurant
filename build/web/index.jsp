<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : index
    Created on : Oct 4, 2019, 9:50:52 PM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">
    <head>

        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sea Dog</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="css/customer.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>
        <jsp:useBean id="p" class="bean.PagingBean" scope="session"/>
        <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="img/logo.png" align="center" width="50" height="50"/> Sea Dog</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" 
                        data-target="#mynav" aria-controls="mynav" aria-expand="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mynav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item px-2 active">
                            <c:if test="${empty sessionScope.login}">
                            <a class="nav-link" href="reservation.jsp">Reservations</a>
                            </c:if>
                            <c:if test="${not empty sessionScope.login}">
                                <c:if test="${sessionScope.login.role=='admin'}">
                              <a class="nav-link" href="/res/admin/reservation.jsp">Reservations</a>  
                                </c:if>
                                <c:if test="${sessionScope.login.role!='admin'}">
                              <a class="nav-link" href="/res/user/reservation.jsp">Reservations</a>  
                                </c:if>
                            </c:if>
                            </li>
                        <li class="nav-item px-2 active">
                            <a class="nav-link" href="" data-toggle="modal"
                               data-target=".menusModal">Menus</a>
                        </li>
                        <li class="nav-item px-2 active">
                            <a class="nav-link" href="" data-toggle="modal"
                               data-target=".drinksModal" >Drinks</a>
                        </li>
                        <li class="nav-item px-2 active">
                            <a class="nav-link" href="" data-toggle="modal" 
                               data-target=".eventModel">Private Dining</a>
                        </li>
                        <li class="nav-item px-2 active">
                            <a class="nav-link" href="#find">Find Us</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--landing-->
        <section id="landing">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1 class="text-light text-center text-capitalize mx-auto
                            "> the sea dog - dundee's finest<br>
                            <small>
                                EST-2019
                            </small>
                        </h1>
                        <p class="text-light text-center text-capitalize mx-auto">
                            <small>
                                MONDAY-FRIDAY
                                <br>5-10pm
                                <br>SATURDAY
                                <br>11am-4pm/5pm-10pm
                                <br>SUNDAY
                                <br>11am-4pm/5pm-9pm
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <!--Menus modal-->
        <div class="modal fade menusModal menusM" tabindex="-1" role="dialog" aria-labledby="myLargeModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content menus-all">
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h1 class="display-4 text-uppercase text-light mb-0">
                                    <strong class="text-dark">
                                        Appetizer!!!
                                    </strong>
                                </h1>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">Welcome to Sea Dog</p>
                            </div>   
                        </div>
                    </div>
                    <!--carousel-->
                    <section id="carousel drinks-carousel">
                        <div id="carouselFull1" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselIndicators" data-slide-to="2"></li>
                                <li data-target="#carouselIndicators" data-slide-to="3"></li>
                                <li data-target="#carouselIndicators" data-slide-to="4"></li>
                                <li data-target="#carouselIndicators" data-slide-to="5"></li>
                                <li data-target="#carouselIndicators" data-slide-to="6"></li>
                                <li data-target="#carouselIndicators" data-slide-to="7"></li>
                                <li data-target="#carouselIndicators" data-slide-to="8"></li>
                            </ol>

                            <!-- The slide show -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img  class="d-block w-100 img-fluid" src="img/appetizer1.jpg" alt="frist">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Savory Party Bread</h5>

                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/appetizer2.jpg" alt="second">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Sweet & Spicy Jalapeno Poppers</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/appetizer3.jpg" alt="third">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Cucumber-Stuffed Cherry  Tomatoes</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/appetizer4.jpg" alt="four">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Glazed Chicken Wings</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/appetizer5.jpg" alt="five">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Almond-Bacon Cheese Crostini</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/appetizer6.jpg" alt="six">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Taco Meatball Ring</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/appetizer7.jpg" alt="seven">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Best Deviled Eggs</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/appetizer8.jpg" alt="eight">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Homemade Guacamole</h5>
                                    </div>
                                </div>
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#carouselFull1" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselFull1" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>

                            </div>
                        </div>              
                    </section>
                    <section id="menu">
                        <div class="container">
                            <div class="row">
                                <!--appetizer menu-->
                                <div class="col-md-6">
                                    <!--single item-->
                                    <c:forEach var="i" items="${p.menuitems}" begin="0" end="3">
                                        <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                            <div class="single-item-text">
                                                <br>
                                                <h5 class="text-uppercase text-dark">${i.itemName}</h5>
                                                <h7 class="text-muted">${i.itemDes}</h7>
                                            </div>
                                            <div class="single-item-price align-self-end">
                                                <h1 class="text-uppercase text-dark">$${i.price}</h1>
                                            </div>
                                            <h4 class="special-text text-capitalize">Apptizers</h4>
                                        </div>
                                    </c:forEach>
                                </div>

                                <div class="col-md-6">
                                    <c:forEach var="i" items="${p.menuitems}" begin="4" end="7">

                                        <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                            <div class="single-item-text">
                                                <br>
                                                <h5 class="text-uppercase text-dark">${i.itemName}</h5>
                                                <h7 class="text-muted">${i.itemDes}</h7>
                                            </div>
                                            <div class="single-item-price align-self-end">
                                                <h1 class="text-uppercase text-dark">$${i.price}</h1>
                                            </div>
                                            <h4 class="special-text text-capitalize">Apptizers</h4>
                                        </div>
                                    </c:forEach>



                                </div>
                            </div>

                    </section>
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h1 class="display-4 text-uppercase text-light mb-0">
                                    <strong class="text-dark">
                                        Salad and Soup!!!
                                    </strong>
                                </h1>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">Welcome to Sea Dog</p>
                            </div>   
                        </div>
                    </div>
                    <!--carousel-->
                    <section id="carousel drinks-carousel">
                        <div id="carouselFull2" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselIndicators" data-slide-to="9" class="active"></li>
                                <li data-target="#carouselIndicators" data-slide-to="10"></li>
                                <li data-target="#carouselIndicators" data-slide-to="11"></li>
                                <li data-target="#carouselIndicators" data-slide-to="12"></li>
                                <li data-target="#carouselIndicators" data-slide-to="13"></li>
                                <li data-target="#carouselIndicators" data-slide-to="14"></li>
                                <li data-target="#carouselIndicators" data-slide-to="15"></li>
                                <li data-target="#carouselIndicators" data-slide-to="16"></li>
                                <li data-target="#carouselIndicators" data-slide-to="17"></li>
                            </ol>

                            <!-- The slide show -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img  class="d-block w-100 img-fluid" src="img/salad1.jpg" alt="frist">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Chicken Croissant Sandwiches</h5>

                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/soup1.jpg" alt="second">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>VPotato Soup</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/salad2.jpg" alt="third">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Veggie Steak Salad</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/soup2.jpg" alt="four">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Easy Pork Posole</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/salad3.jpg" alt="five">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Slow-Cooker Chicken Taco</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/soup3.jpg" alt="six">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Seafood Cioppino</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/salad4.jpg" alt="seven">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Salmon Caesar Salad</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/soup4.jpg" alt="eight">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Chunky Creamy Chicken Soup</h5>
                                    </div>
                                </div>
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#carouselFull2" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselFull2" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>

                            </div>
                        </div>              
                    </section>
                    <jsp:setProperty name="p" property="size" value="16"/>
                    <section id="menu">
                        <div class="container">
                            <div class="row">
                                <!--appetizer menu-->
                                <div class="col-md-6">
                                    <!--single item-->
                                    <c:forEach var="i" items="${p.menuitems}" begin="8" end="11">
                                        <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                            <div class="single-item-text">
                                                <br>
                                                <h5 class="text-uppercase text-dark">${i.itemName}</h5>
                                                <h7 class="text-muted">${i.itemDes}</h7>
                                            </div>
                                            <div class="single-item-price align-self-end">
                                                <h1 class="text-uppercase text-dark">$${i.price}</h1>
                                            </div>
                                            <h4 class="special-text text-capitalize">Salad and Soup</h4>
                                        </div>
                                    </c:forEach>
                                </div>

                                <div class="col-md-6">
                                    <c:forEach var="i" items="${p.menuitems}" begin="12" end="16">

                                        <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                            <div class="single-item-text">
                                                <br>
                                                <h5 class="text-uppercase text-dark">${i.itemName}</h5>
                                                <h7 class="text-muted">${i.itemDes}</h7>
                                            </div>
                                            <div class="single-item-price align-self-end">
                                                <h1 class="text-uppercase text-dark">$${i.price}</h1>
                                            </div>
                                            <h4 class="special-text text-capitalize">Salad and Soup</h4>
                                        </div>
                                    </c:forEach>



                                </div>
                            </div>

                    </section>
                    <!--Main Meal-->
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h1 class="display-4 text-uppercase text-light mb-0">
                                    <strong class="text-dark">
                                        Main Meal!!!
                                    </strong>
                                </h1>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">Welcome to Sea Dog</p>
                            </div>   
                        </div>
                    </div>
                    <!--carousel-->
                    <section id="carousel drinks-carousel">
                        <div id="carouselFull3" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselIndicators" data-slide-to="18" class="active"></li>
                                <li data-target="#carouselIndicators" data-slide-to="19"></li>
                                <li data-target="#carouselIndicators" data-slide-to="20"></li>
                                <li data-target="#carouselIndicators" data-slide-to="21"></li>
                                <li data-target="#carouselIndicators" data-slide-to="22"></li>
                                <li data-target="#carouselIndicators" data-slide-to="23"></li>
                                <li data-target="#carouselIndicators" data-slide-to="24"></li>
                                <li data-target="#carouselIndicators" data-slide-to="25"></li>
                                <li data-target="#carouselIndicators" data-slide-to="26"></li>
                            </ol>

                            <!-- The slide show -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img  class="d-block w-100 img-fluid" src="img/main1.jpg" alt="frist">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Simple Shrimp Pad Thai</h5>

                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/main2.jpg" alt="second">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Beer-Steamed Mussels with Chorizo</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/main3.jpg" alt="third">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Pork Chops with Celery  </h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/main4.jpg" alt="four">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Pork Chops with Radishes and Charred Scallions</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/main5.jpg" alt="five">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Red Snapper With Coconut-Clam Broth</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/main6.jpg" alt="six">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Brisket with Pomegranate-Walnut Sauce and Pistachio Gremolata</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/main7.jpg" alt="seven">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Roast Beef Tenderloin with Caesar Crust</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/main8.jpg" alt="eight">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Prosciutto-Stuffed Chicken with Mushroom Sauce</h5>
                                    </div>
                                </div>
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#carouselFull3" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselFull3" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>

                            </div>
                        </div>              
                    </section>
                    <jsp:setProperty name="p" property="size" value="24"/>
                    <section id="menu">
                        <div class="container">
                            <div class="row">
                                <!--appetizer menu-->
                                <div class="col-md-6">
                                    <!--single item-->
                                    <c:forEach var="i" items="${p.menuitems}" begin="16" end="19">
                                        <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                            <div class="single-item-text">
                                                <br>
                                                <h5 class="text-uppercase text-dark">${i.itemName}</h5>
                                                <h7 class="text-muted">${i.itemDes}</h7>
                                            </div>
                                            <div class="single-item-price align-self-end">
                                                <h1 class="text-uppercase text-dark">$${i.price}</h1>
                                            </div>
                                            <h4 class="special-text text-capitalize">Main Meal</h4>
                                        </div>
                                    </c:forEach>
                                </div>

                                <div class="col-md-6">
                                    <c:forEach var="i" items="${p.menuitems}" begin="20" end="23">

                                        <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                            <div class="single-item-text">
                                                <br>
                                                <h5 class="text-uppercase text-dark">${i.itemName}</h5>
                                                <h7 class="text-muted">${i.itemDes}</h7>
                                            </div>
                                            <div class="single-item-price align-self-end">
                                                <h1 class="text-uppercase text-dark">$${i.price}</h1>
                                            </div>
                                            <h4 class="special-text text-capitalize">Main Meal</h4>
                                        </div>
                                    </c:forEach>



                                </div>
                            </div>

                    </section>
                    <!--Dessert-->
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h1 class="display-4 text-uppercase text-light mb-0">
                                    <strong class="text-dark">
                                        Dessert!!!
                                    </strong>
                                </h1>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">Welcome to Sea Dog</p>
                            </div>   
                        </div>
                    </div>
                    <!--carousel-->
                    <section id="carousel drinks-carousel">
                        <div id="carouselFull4" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselIndicators" data-slide-to="27" class="active"></li>
                                <li data-target="#carouselIndicators" data-slide-to="28"></li>
                                <li data-target="#carouselIndicators" data-slide-to="29"></li>
                                <li data-target="#carouselIndicators" data-slide-to="30"></li>
                                <li data-target="#carouselIndicators" data-slide-to="31"></li>
                                <li data-target="#carouselIndicators" data-slide-to="32"></li>
                                <li data-target="#carouselIndicators" data-slide-to="33"></li>
                                <li data-target="#carouselIndicators" data-slide-to="34"></li>
                                <li data-target="#carouselIndicators" data-slide-to="35"></li>
                            </ol>

                            <!-- The slide show -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img  class="d-block w-100 img-fluid" src="img/dessert1.jpg" alt="frist">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Simple Shrimp Pad Thai</h5>

                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/dessert2.jpg" alt="second">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Beer-Steamed Mussels with Chorizo</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/dessert3.jpg" alt="third">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Pork Chops with Celery  </h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/dessert4.jpg" alt="four">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Pork Chops with Radishes and Charred Scallions</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/dessert5.jpg" alt="five">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Red Snapper With Coconut-Clam Broth</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/dessert6.jpg" alt="six">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Brisket with Pomegranate-Walnut Sauce and Pistachio Gremolata</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/dessert7.jpg" alt="seven">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Roast Beef Tenderloin with Caesar Crust</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/dessert8.jpg" alt="eight">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Prosciutto-Stuffed Chicken with Mushroom Sauce</h5>
                                    </div>
                                </div>
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#carouselFull4" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselFull4" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>

                            </div>
                        </div>              
                    </section>
                    <jsp:setProperty name="p" property="size" value="32"/>
                    <section id="menu">
                        <div class="container">
                            <div class="row">
                                <!--appetizer menu-->
                                <div class="col-md-6">
                                    <!--single item-->
                                    <c:forEach var="i" items="${p.menuitems}" begin="24" end="27">
                                        <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                            <div class="single-item-text">
                                                <br>
                                                <h5 class="text-uppercase text-dark">${i.itemName}</h5>
                                                <h7 class="text-muted">${i.itemDes}</h7>
                                            </div>
                                            <div class="single-item-price align-self-end">
                                                <h1 class="text-uppercase text-dark">$${i.price}</h1>
                                            </div>
                                            <h4 class="special-text text-capitalize">Dessert</h4>
                                        </div>
                                    </c:forEach>
                                </div>

                                <div class="col-md-6">
                                    <c:forEach var="i" items="${p.menuitems}" begin="28" end="31">

                                        <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                            <div class="single-item-text">
                                                <br>
                                                <h5 class="text-uppercase text-dark">${i.itemName}</h5>
                                                <h7 class="text-muted">${i.itemDes}</h7>
                                            </div>
                                            <div class="single-item-price align-self-end">
                                                <h1 class="text-uppercase text-dark">$${i.price}</h1>
                                            </div>
                                            <h4 class="special-text text-capitalize">Dessert</h4>
                                        </div>
                                    </c:forEach>

                                </div>
                            </div>
                            <!--button Close-->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-event-Modal" data-dismiss="modal" style="background-color: #53131e; border-color: #ffff00; color: #ffcccc;">Close</button>
                            </div>
                    </section>



                </div>

            </div>
        </div>
        <!--Drinks modal-->
        <div class="modal fade drinksModal drinksM" tabindex="-1" role="dialog" aria-labledby="myLargeModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content drinks-all">
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h1 class="display-4 text-uppercase text-light mb-0">
                                    <strong class="text-dark">
                                        Top Shelf Drinks
                                    </strong>
                                </h1>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">Welcome to Sea Dog</p>
                            </div>   
                        </div>
                    </div>
                    <!--Slick-->
                    <div class="slider slider-for">
                        <div>
                            <img src="img/drink1.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div>
                            <img src="img/drink2.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div>
                            <img src="img/drink3.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div>
                            <img src="img/drink4.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div>
                            <img src="img/drink5.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div>
                            <img src="img/drink6.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div>
                            <img src="img/drink7.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div>
                            <img src="img/drink8.jpg" alt="picture of bar" class="img-fluid">
                        </div>

                    </div>
                    <div class="slider slider-nav" >
                        <div class="pr-1">
                            <img src="img/drink1.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div class="pr-1">
                            <img src="img/drink2.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div class="pr-1">
                            <img src="img/drink3.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div class="pr-1">
                            <img src="img/drink4.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div class="pr-1">
                            <img src="img/drink5.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div class="pr-1">
                            <img src="img/drink6.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div class="pr-1">
                            <img src="img/drink7.jpg" alt="picture of bar" class="img-fluid">
                        </div>
                        <div class="pr-1">
                            <img src="img/drink8.jpg" alt="picture of bar" class="img-fluid">
                        </div>

                    </div>

                    <!--button Close-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-drink-Modal" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
        <!--Private Event-->

        <div class="modal fade eventModel eventM" tabindex="-1" role="dialog" aria-labledby="myLargeModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content event-all">
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h1 class="display-4 text-uppercase text-light mb-0">
                                    <strong class="text-dark">
                                        Private Dining!!!
                                    </strong>
                                </h1>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">The private party room at Sea Dog is an intimate dining and events space located in the world’s first Coca-Cola bottling room, and offers the opportunity for a customized and exclusive experience for up to 100 guests. </p>
                            </div>   
                        </div>
                    </div>
                    <!--carousel-->
                    <section id="carousel drinks-carousel">
                        <div id="carouselFull" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselIndicators" data-slide-to="2"></li>
                                <li data-target="#carouselIndicators" data-slide-to="3"></li>
                                <li data-target="#carouselIndicators" data-slide-to="4"></li>
                                <li data-target="#carouselIndicators" data-slide-to="5"></li>
                            </ol>

                            <!-- The slide show -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img  class="d-block w-100 img-fluid" src="img/space0.jpg" alt="first">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Come with us</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/space1.jpg" alt="second">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Come with us</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/space3.jpg" alt="third">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Come with us</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/space2.jpg" alt="four">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Come with us</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/space4.jpg" alt="five">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Come with us</h5>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img  class="d-block w-100 img-fluid" src="img/space5.jpg" alt="six">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Come with us</h5>
                                    </div>
                                </div>

                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#carouselFull" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselFull" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>

                            </div>
                        </div>              
                    </section>
                    <!--button Close-->
                    <div class="modal-footer">
                        <p align="right"><em>For additional information, please contact: <br> 423.266.1121</em></p>
                        <a href="mailto:an@easybistro.com?cc=mr@easybistro.com&subject=Private%20Dining%20Inquiry" class="btn btn-drink-Modal">Contact</a>
                    </div>



                </div>
            </div>
        </div>

        <!--Find us-->
        <form action="account" method="post">
            <section id="find">
                <!--Sign-up-->
                <section class="sign-up" id="sign-up">

                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-6 text-center">
                                <h2>SIGN UP FOR OUR NEWSLETTER
                                    TO GET THE LATEST NEWS</h2>
                                <p class="text-light"><small>
                                        Join the E-Club, be the first to know about menu changes, exclusive offers and more...
                                    </small><br>(423.266.1121)</p>
                            </div>
                            <div class="d-none d-lg-block col-lg-12 col-xl-6">
                                <form class="form-inline d-flex justify-content-center"
                                      enctype="multipart/form-data" role="form">
                                    <div class="form-group p-2">
                                        <label for="exampleInputName2"></label>
                                        <input name="nameFind" type="text" class="form-control"
                                               id="exampleInputName2" placeholder="Your Name..." required>
                                    </div>
                                    <div class="form-group p-2 pr-3">
                                        <label for="exampleInputEmail2"></label>
                                        <input name="emailFind" type="form-control" class="form-control"
                                               id="exampleInputEmail2" placeholder="Your Email..." required>

                                    </div>
                                    <button type="submit" class="btn btn-sign" name="submitFind">Sign-up</button>
                                
                                <table align="right">
                                    <tr>
                                        <td>
                                            <a href="https://www.facebook.com/easybistro/" >
                                                <img src="img/facebook.png" width="50px" height="50px">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="https://twitter.com/easybistro?lang=en" target="_blank">
                                                <img src="img/twitter.png" width="50px" height="50px">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="https://www.instagram.com/easybistro/" target="_blank">
                                                <img src="img/instagram.png" width="50px" height="50px">
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>

                </section>
            </section>
        </form>






        <!--Footer-->
        <div class="container-fluid text-center footer">
            <div class="row" >
                <div class="mx-auto">
                    <div class="blogo col-xs-12 col-sm-12 col-md-12 col-lg-4">
                        <a href="#" class="btn">Sea Dog</a>
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="footer-icons"></div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="copyright mx-auto Name">
                        <a class="btn" href="https://www.google.com/maps/dir/''/easy+bistro/@35.0544632,-85.3809869,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x88605e6174eff3c5:0x3d4ed662adf5856a!2m2!1d-85.3109468!2d35.0544833" >&copy;203 Broad St, Chattanooga, TN 37402</a>

                    </div>
                </div>
            </div>

        </div>
        <!--End-->
        <div id="bttp" class="btt">
            <a href="#"><i class="fas fa-arrow-circle-up"></i></a>
        </div>




        <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js"></script>
        <script src="js/Myscript.js"></script>
    </body>
</html>
