<%-- 
    Document   : footer
    Created on : Oct 1, 2019, 10:24:57 PM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="css/both.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>
        <!--Footer-->
        <div class="container-fluid text-center footer">
            <div class="row" >
                <div class="mx-auto">
                    <div class="blogo col-xs-12 col-sm-12 col-md-12 col-lg-4">
                        <a href="index.jsp" class="btn">Sea Dog</a>
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="footer-icons"></div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="copyright mx-auto Name">
                        <a class="btn" href="https://www.google.com/maps/dir/''/easy+bistro/@35.0544632,-85.3809869,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x88605e6174eff3c5:0x3d4ed662adf5856a!2m2!1d-85.3109468!2d35.0544833" >&copy;203 Broad St, Chattanooga, TN 37402</a>
                        
                    </div>
                </div>
            </div>

        </div>
        <!--End-->
        <div id="bttp" class="btt">
            <a href="#"><i class="fas fa-arrow-circle-up"></i></a>
        </div>
    </body>
</html>
