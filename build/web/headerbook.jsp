<%-- 
    Document   : headerbook
    Created on : Oct 24, 2019, 11:24:23 PM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="css/both.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="index.jsp">
                    <img src="img/logo.png" align="center" width="50" height="50"/> Sea Dog</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" 
                        data-target="#mynav" aria-controls="mynav" aria-expand="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mynav">
                    <ul class="navbar-nav ml-auto">
                        <table align="left">
                            <tr>
                                <td>
                                    <a href="https://www.facebook.com/easybistro/" >
                                        <img src="img/facebook.png" width="50px" height="50px">
                                    </a>
                                </td>
                                <td>
                                    <a href="https://twitter.com/easybistro?lang=en" target="_blank">
                                        <img src="img/twitter.png" width="50px" height="50px">
                                    </a>
                                </td>
                                <td>
                                    <a href="https://www.instagram.com/easybistro/" target="_blank">
                                        <img src="img/instagram.png" width="50px" height="50px">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </ul>
                </div>
            </div>
        </nav>

    </body>
</html>
