<%-- 
    Document   : info
    Created on : Oct 28, 2019, 11:12:39 PM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Infomation of You</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="../css/both.css" rel="stylesheet" type="text/css"/>

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>td.indent{padding-left: 9.5em}</style>

    </head>
    <body>
        <jsp:include page="../user/headeruser.jsp"></jsp:include>
            <!--Update Table modal-->
        <jsp:useBean id="update" class="dao.UpdateInfoDAO" scope="session"></jsp:useBean>
            <!--Section title-->
            <div class="container">
                <div class="row py-5">
                    <div class="col text-center">
                        <h3 class="text-dark" style="font-weight: bold">
                            Information of Table you have booked
                        </h3>
                        <div class="title-underlind mx-auto"></div>
                        <p class="mt-2 text-capitalize text-dark">You can following information Table you have booked</p>
                    </div>   
                </div>
            </div>
        <jsp:setProperty name="update" property="email" value="${sessionScope.login.email}"></jsp:setProperty>
            <div class="container">
                <div class="row py-5">
                    <div class="col text-center">

                        <table class="edit" border="1"  cellspacings="0" align="center">
                            <tr>
                                <th></th>
                                <th>Time</th>
                                <th>Place</th>
                                <th>Number Guest</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                        <c:forEach var="j" begin="0" end="${update.tableSize-1}">   
                            <jsp:setProperty name="update" property="index" value="${j}"></jsp:setProperty>
                            <c:if test="${not empty update.bookingtable}">
                            <tr>     
                                <td>${update.mealitem.name}</td>
                                <td>${update.mealitem.timem}</td>
                                <td>${update.nameType}</td>
                                <td>${update.bookingtable.guest}</td>
                                <td>${update.bookingtable.day}</td>
                                <c:if test="${update.checktime}">
                                <td>
                                    <a href="../cancel?deltab=${update.bookingtable.tableid}&delcus=${update.bookingtable.customerid}">
                                     <button type="button" class="btn btn-drink-Modal">Cancel</button>
                                    </a>
                                </td>
                                </c:if>
                            </tr>
                            </c:if>
                        </c:forEach>
                    </table>
                </div>
            </div>

        </div>
        <form action="../updateuser" method="get" name="upinfo">
            <div align="center">
                <div class="container">
                    <div class="row py-5">
                        <div class="col text-center">
                            <h3 class="text-dark" style="font-weight: bold">
                                Information of Customer
                            </h3>
                            <div class="title-underlind mx-auto"></div>
                        </div>   
                    </div>
                </div>
                <table class="single-item d-flex justify-content-between my-3 p-3 ">
                    <tr class="single-item-text">
                        <td class="text-uppercase text-dark " style=" font-weight: bold">Your Name:</td>
                        <td style="padding-left: 4.2em">
                            <div class="col-sm-10">
                                <div class="form-group p-2">
                                    <label for="exampleInputName2"></label>
                                    <input name="nameCus" type="text" class="form-control"
                                           id="exampleInputName2" placeholder="Your Name..." size="70" value="${update.infoCustomer.customerName}">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="single-item-text">
                        <td class="text-uppercase text-dark " style=" font-weight: bold">Phone: </td>
                        <td style="padding-left: 4.2em">
                            <div class="col-sm-10">
                                <div class="form-group p-2">
                                    <label for="exampleInputName2"></label>
                                    <input name="phoneCus" type="text" class="form-control"
                                           id="exampleInputName2" placeholder="Your Phone..."  value="${update.infoCustomer.phone}" >
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="single-item-text">
                        <td class="text-uppercase text-dark " style=" font-weight: bold">Email: </td>
                        <td style="padding-left: 4.2em">
                            <div class="col-sm-10">
                                <div class="form-group p-2">
                                    <label for="exampleInputName2"></label>
                                    <input name="emailCus" type="text" class="form-control"
                                           id="exampleInputName2" placeholder="Your Email..." value="${update.infoCustomer.email}" disabled>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="single-item-text">
                        <td class="text-uppercase text-dark " style=" font-weight: bold">Note: </td>
                        <td style="padding-left: 4.2em">
                            <div class="col-sm-10">
                                <div class="form-group p-2">
                                    <label for="exampleInputName2"></label>
                                    <textarea name="noteCus" type="text" class="form-control"
                                              id="exampleInputName2"  value="${update.infoCustomer.note}" >${update.infoCustomer.note}</textarea>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!--button Notify Me-->
                <!--button Submit-->

                <div class="input-group-addon nav-item px-2 active">
                    <button type="submit" onclick="document.forms['upinfo'].submit();" name="submitInfo" value="${update.infoCustomer.email}" class="btn btn-drink-Modal" style="font-size: 24px"/>
                         Update Information</button>
                </div>          
            </div>
        </form>
    </body>
</html>
