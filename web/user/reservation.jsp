<%-- 
    Document   : header
    Created on : Sep 21, 2019, 5:40:44 PM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reservation</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="../css/both.css" rel="stylesheet" type="text/css"/>

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>td.indent{padding-left: 9.5em}</style>

    </head>
    <body>
        <jsp:include page="../user/headeruser.jsp"></jsp:include>
            <form id="choisedate1" name="choisedate1" > 
            <jsp:useBean id="table" class="dao.BookingTableDAO" scope="session"></jsp:useBean>
                <div class="container">
                    <div class="row py-5">
                        <div class="col text-center" >
                            <h5 class="display-4 text-uppercase text-light mb-0">
                                <strong class="text-dark">
                                    Sea Dog
                                </strong>
                            </h5>
                            <div class="title-underlind mx-auto"></div>

                            <table class="single-item d-flex justify-content-between my-3 p-3" >
                                <tr class="single-item-text ">
                                    <td class=" text-uppercase text-dark " >Need to know&emsp;</td>
                                    <td><p style="color: #53131e">For specific seating requests, please call Easy Bistro. We will always do our best to accommodate the requests, however we cannot guarantee that specific seating will be available at the time of visit.

                                            Please call for parties of 9 and up. Reservations will be held for 15 minutes before being released to walk-ins. Please call 423-266-1121 if you are running late. Valet Parking Available $5.</p></td>
                                </tr>
                            </table>

                            <table class="single-item d-flex justify-content-between my-3 p-3 ">
                                <tr class="single-item-text">
                                    <td class="text-uppercase text-dark " style=" font-weight: bold">Select Date:</td>
                                    <td style="padding-left: 4.2em">
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <button type="button" class="btn btn-outline-secondary docs-datepicker-trigger" disabled>
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </button>
                                                </div>
                                                <input class="form-control" id="date" name="date" placeholder="DD/MM/YYYY" onchange="document.forms['choisedate1'].submit();" type="text" value="${param.date}" />

                                        </div>
                                    </div>


                                </td>

                                <td class="text-uppercase text-dark " style=" font-weight: bold">Guests:&emsp;&emsp;&ensp;</td>
                                <td style="padding-left: 4.3em">
                                    <div class="input-group mb-3">
                                        <select class="custom-select" id="inputGroupSelect" onchange="document.forms['choisedate1'].submit();" name="guest">
                                            <option value="1" ${param.guest ==1?"selected":""}>1 Guests</option>
                                            <option value="2" ${param.guest ==2?"selected":""}>2 Guests</option>
                                            <option value="3" ${param.guest ==3?"selected":""}>3 Guests</option>
                                            <option value="4" ${param.guest ==4?"selected":""}>4 Guests</option>
                                            <option value="5" ${param.guest ==5?"selected":""}>5 Guests</option>
                                            <option value="6" ${param.guest ==6?"selected":""}>6 Guests</option>
                                            <option value="7" ${param.guest ==7?"selected":""}>7 Guests</option>
                                            <option value="8" ${param.guest ==8?"selected":""}>8 Guests</option>
                                            <option value="9" ${param.guest ==9?"selected":""}>9+ Guests</option>
                                        </select>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-outline-secondary docs-datepicker-trigger" disabled>
                                                <img src="../img/guest.png" width="20px" height="20px"> 
                                            </button>

                                        </div>
                                    </div>

                                </td>
                                <td class="indent"></td>
                                <td>
                                    <div class="input-group-addon nav-item px-2 active">
                                        <c:if test="${not empty sessionScope.login}">
                                            <!--button Notify Me-->
                                            <a href="../user/info.jsp">
                                                    <button type="button" class="btn btn-drink-Modal" style="background-color: #53131e; color: #ffcccc;">
                                                        Table you have booked</button>
                                                </a>
                                        </c:if>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>   
                </div>
            </div>
            <jsp:setProperty name="table" property="guest" value="${param.guest}"/>           
            <jsp:setProperty name="table" property="day" value="${param.date}"/>
            <!--Table button-->
            <section >
                <div class="container">
                    <div class="row">
                        <!--appetizer menu-->
                        <div class="col-md-6">
                            <!--single item-->
                            <jsp:setProperty name="table" property="index" value="0"/>
                            <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                <table>
                                    <c:forEach var="j" begin="1" end="${table.sizelist }" >
                                        <tr>
                                            <c:forEach var="i" items="${table.listsmeal1}" begin="${table.index}" end="${table.index+5}">
                                                <!--button Close-->
                                                <td>
                                                    
                                                    <a href="../book?tablebook=${i.tableid}&email=${sessionScope.login.email}&day=${param.date}&guest=${param.guest}" >
                                                        <button type="button" class="btn btn-event-Modal" style="background-color: #53131e; border-color: #ffff00; color: #ffcccc; height: 80px; width: 80px" name="tableid" value="${i.tableid}">
                                                            ${i.timem}
                                                            <jsp:setProperty name="table" property="type" value="${i.idType}"/>
                                                            <p style="font-size: 9px">${table.nametype}</p>
                                                        </button>
                                                    </a>
                                                </td>

                                            </c:forEach>
                                        </tr>
                                        <jsp:setProperty name="table" property="index" value="${j*6}"/>
                                    </c:forEach>
                                </table>

                                <h4 class="special-text text-capitalize" style="font-size: 12px">BRUNCH</h4>
                            </div>
                        </div>
                        <jsp:setProperty name="table" property="index" value="0"/>
                        <div class="col-md-6">

                            <div class="single-item d-flex justify-content-between my-3 p-3 special">
                                <table>
                                    <c:forEach var="j" begin="1" end="${table.size}"  >
                                        <tr>
                                            <c:forEach var="i" items="${table.listmeal2}" begin="${table.index}" end="${table.index+5}">
                                                <!--button Close-->
                                                <td>
                                                    <a href="../book?tablebook=${i.tableid}&email=${sessionScope.login.email}&day=${table.day}&guest=${table.guest}">
                                                        <button type="button" class="btn btn-event-Modal" style="background-color: #53131e; border-color: #ffff00; color: #ffcccc; height: 80px; width: 80px" >
                                                            ${i.timem}
                                                            <jsp:setProperty name="table" property="type" value="${i.idType}"/>
                                                            <p style="font-size: 9px">${table.nametype}</p>
                                                        </button>
                                                    </a>
                                                </td>

                                            </c:forEach>
                                        </tr>
                                        <jsp:setProperty name="table" property="index" value="${j*6}"/>
                                    </c:forEach>
                                </table>
                                <h4 class="special-text text-capitalize" style="font-size: 12px" >DINNER</h4>                          
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>

        <jsp:include page="../user/footeruser.jsp"></jsp:include>





        <!-- Include jQuery -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

        <script type="text/javascript">
                                            $(document).ready(function () {
                                                var date_input = $('input[name="date"]'); //our date input has the name "date"
                                                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                                                date_input.datepicker({
                                                    format: 'dd/mm/yyyy',
                                                    container: container,
                                                    todayHighlight: true,
                                                    autoclose: true,
                                                    startDate: '0d',
                                                    endDate: '+2m'

                                                });

                                            })

        </script>
    </body>
</html>
<script>
    function loadpick()
    {
        document.choisedate1.submit()
    }
</script>


