<%-- 
    Document   : header
    Created on : Oct 12, 2019, 12:18:17 AM
    Author     : Envy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.min.css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link href="/res/css/both.css" rel="stylesheet" type="text/css"/>
        <style>
            td.indent{padding-left: 12.5em}
            tr.indent{padding-bottom:  20.5em}
            td.cal{font-size: 14px ;color: #fffacc}
        </style>
    </head>
    <body>
        <jsp:useBean id="tab" class="dao.TableDAO" scope="session"></jsp:useBean>
            <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="/res/index.jsp">
                        <img src="/res/img/logo.png" align="center" width="50" height="50"/> Sea Dog</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" 
                            data-target="#mynav" aria-controls="mynav" aria-expand="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="mynav"> 
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item px-2 active">
                                <a class="nav-link" href="" data-toggle="modal"
                                   data-target=".menusModal">Management Table</a>
                            </li>
                        </ul>
                    <c:if test="${empty sessionScope.login}">
                        <ul class="navbar-nav ml-auto">
                            <button type="button" class="btn btn-event-Modal" data-toggle="modal"
                                    data-target=".loginModel" style="background-color: #53131e; color: #ffcccc;">
                                LOG IN</button>
                        </ul>
                    </c:if>
                    <c:if test="${not empty sessionScope.login}">
                        <ul class="navbar-nav ml-auto">

                            <small style="color: #ffcccc; font-size: 16px"> Welcome ${sessionScope.login.name} &emsp;</small>
                            <a href="/res/logout">
                                <button type="button" class="btn btn-event-Modal" style="background-color: #53131e; color: #ffcccc;" >
                                    LOG OUT</button></a>
                        </ul>
                    </c:if>                    
                </div>
            </div>
        </nav>

        <!--Create table-->

        <div class="modal fade menusModal menusM" tabindex="-1" role="dialog" aria-labledby="myLargeModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content drinks-all">
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h3 class="text-dark" style="font-weight: bold">
                                    Information of table in restaurant Sea Dog
                                </h3>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">You can insert and delete table in Sea Dog web </p>

                            </div>   
                        </div>
                    </div>
                        <section class="sign-up" id="sign-up">
                            <div class="container">
                                <div class="row py-5">
                                    <div class="col text-center">
                                        <table class="edit" border="1"  cellspacings="0" style="width: 100%">
                                            <tr>
                                                <th>Table ID</th>
                                                <th>Name Meal/th>
                                                <th>Time</th>
                                                <th>Place</th>   
                                                <th></th>
                                            </tr>
                                            <c:forEach var="i" items="${tab.list}">
                                                <tr>
                                                    <td>${i.tableid}</td>
                                                    <td>${i.name}</td>
                                                    <td>${i.timem}</td>
                                                    <td>${i.typeName}</td>
                                                    <td>
                                                        <a href="../deletable?deletetable=${i.tableid}">
                                                        <button type="button" name="deletetable" class="btn btn-event-Modal" onclick="return confirm('Are you sure that delete table in Sea Dog restaurant');" style="background-color: #53131e; border-color: #ffff00; color: #ffcccc;">
                                                            Delete Table
                                                        </button>
                                                        </a>      
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </table>

                                    </div>
                                </div>

                            </div>
                        </section>
                    
                    <!--button Close-->
                    <div class="modal-footer" align="center" >
                        <em>Do you want create table in restaurant?</em>
                        <button type="button" class="btn btn-event-Modal" data-dismiss="modal" data-toggle="modal" data-target=".eventModel" style="background-color: #53131e; border-color: #ffff00; color: #ffcccc;">Create Table</button>
                    </div>

                </div>
            </div>
        </div>
        <!---Update table-->
        <div class="modal fade eventModel eventM" tabindex="-1" role="dialog" aria-labledby="myLargeModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content event-all">
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h3 class="text-dark" style="font-weight: bold">
                                    Information of a table in restaurant
                                </h3>
                                <div class="title-underlind mx-auto"></div>

                            </div>   
                        </div>
                    </div>  
                    <form  name="signup" action="../table" method="get">
                        <input type="hidden" name="submitSign" value="Create"/>
                        <section class="sign-up" id="sign-up" align="center">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="d-none d-lg-block col-lg-12 col-xl-6">
                                        <div class="form-group p-3">
                                                    <label for="exampleInputName2"></label>
                                                    <input name="timestart" type="text" class="form-control"
                                                           id="exampleInputName2" placeholder="hh:mm(AM/PM)" style="width: 50vh" required>
                                                </div>
                                        <table >
                                            <tr>
                                                
                                                <td class="form-group p-3" >
                                                    <select name="place" class="form-control" style="width: 25vh" required>
                                                    <c:forEach var="p" items="${tab.alltype}">
                                                        <option value="${p.id}">${p.name}</option>
                                                    </c:forEach>
                                                    </select>
                                                </td>
                                                <td class="form-group p-3">
                                                        <select name="dayweek" class="form-control" style="width: 25vh" required>
                                                            <option value="Monday">Monday</option>
                                                            <option value="Monday" >Tuesday</option>
                                                            <option value="Monday">Wednesday</option>
                                                            <option value="Monday">Thursday</option>
                                                            <option value="Monday">Friday</option>
                                                            <option value="Saturday">Saturday</option>
                                                            <option value="Sunday">Sunday</option>
                                                        </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="cal form-group p-3">
                                                    <input type="radio" name="idmeal" value="1" class="form-control" required/>BRUNCH&emsp;
                                                </td>
                                                <td  class="cal form-group p-3">
                                                    <input type="radio" name="idmeal" value="2" class="form-control" required/>DINNER &emsp;
                                                </td>
                                            </tr>
                                        </table>  
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!--button Submit-->
                        <div class="modal-footer">
                            <input name="submitSign" type="submit" onclick="document.forms['signup'].submit();" value="Create"  class="btn btn-event-Modal" data-dismiss="modal" style="color: #53131e ; width: 100%; height: 100%;font-weight: bold"/>
                        
                    </div>
                    </form>

                </div>
            </div>
        </div>
        <!--Login modal-->

        <div class="modal fade loginModel loginM" tabindex="-1" role="dialog" aria-labledby="myLargeModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content drinks-all">
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h3 class="text-dark" style="font-weight: bold">
                                    Please Login!!! 
                                </h3>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">Sea Dog uses Resy’s hospitality platform to securely manage your account information and reservations.</p>

                            </div>   
                        </div>
                    </div>            

                    <form action="../login" method="post">
                        <section class="sign-up" id="sign-up">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="d-none d-lg-block col-lg-12 col-xl-6">

                                        <table>
                                            <tr>
                                                <td >
                                                    <div class="form-group p-2">
                                                        <label for="exampleInputName2"></label>
                                                        <input name="emailLogin" type="text" class="form-control"
                                                               id="exampleInputName2" placeholder="Your Email..." style="width: 50vh" required>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    <div class="form-group p-2 pr-3">
                                                        <label for="exampleInputEmail2"></label>
                                                        <input name="passLogin" type="password" class="form-control"
                                                               id="exampleInputEmail2" placeholder="Your Password..." style="width: 50vh" required>

                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <button type="submit"  name="submitLogin" class="btn btn-sign">Continute</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <a class="nav-link" href="" data-dismiss="modal" data-toggle="modal"
                                                       data-target=".forgotModal" ><small>Forgot Password?</small></a>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>

                            </div>
                        </section>
                    </form>
                    <!--button Close-->
                    <div class="modal-footer" align="center" >
                        <em>Don’t have an account?</em>
                        <button type="button" class="btn btn-event-Modal" data-dismiss="modal" data-toggle="modal" data-target=".signupModal" style="background-color: #53131e; border-color: #ffff00; color: #ffcccc;">Sign Up</button>
                    </div>

                </div>
            </div>
        </div>

        <!--forgot Modal-->
        <div class="modal fade forgotModal forgotM" tabindex="-1" role="dialog" aria-labledby="myLargeModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content drinks-all">
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h3 class="text-dark" style="font-weight: bold">
                                    RESET PASSWORD:
                                </h3>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">You know the drill. Please start by providing the email address you used to register.</p>
                            </div>   
                        </div>
                    </div>
                    <section class="sign-up" id="sign-up">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="d-none d-lg-block col-lg-12 col-xl-6">
                                    <form class="form-inline d-flex justify-content-center" action="contact.jsp" method="post"
                                          enctype="multipart/form-data" role="form">
                                        <div class="form-group p-2">
                                            <label for="exampleInputName2"></label>
                                            <input name="name" type="text" class="form-control"
                                                   id="exampleInputName2" placeholder="Your Email..." style="width: 50vh" required>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--button Submit-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-event-Modal" data-dismiss="modal" style="background-color: #53131e; border-color: #ffff00; color: #ffcccc; width: 20vh">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Sign Up-->

        <div class="modal fade signupModal signupM" tabindex="-1" role="dialog" aria-labledby="myLargeModal" aria-hidden="true" style="overflow: auto">
            <div class="modal-dialog modal-lg">
                <div class="modal-content drinks-all">
                    <!--Section title-->
                    <div class="container">
                        <div class="row py-5">
                            <div class="col text-center">
                                <h3 class="text-dark" style="font-weight: bold">
                                    Please create an account to continue.
                                </h3>
                                <div class="title-underlind mx-auto"></div>
                                <p class="mt-2 text-capitalize text-dark">Let’s get some basic information so that Easy Bistro & Bar knows who you are.</p>
                            </div>   
                        </div>
                    </div>
                    <form action="../account" method="get" name="signup">
                        <input type="hidden" name="submitSign" value="submitSign"/>
                        <section class="sign-up" id="sign-up">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="d-none d-lg-block col-lg-12 col-xl-6">
                                        <table>
                                            <tr>
                                                <td >
                                                    <div class="form-group p-2">
                                                        <label for="exampleInputName2"></label>
                                                        <input name="nameSign" type="text" class="form-control"
                                                               id="exampleInputName2" placeholder="Your Name..." style="width: 50vh" required>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group p-2 pr-3">
                                                        <label for="exampleInputEmail2"></label>
                                                        <input name="phoneSign" type="text" class="form-control"
                                                               id="exampleInputEmail2" placeholder="Mobile Phone..." style="width: 50vh" required>

                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <small style="color: #fffacc">Sea Dog will only use this number to confirm and manage your reservation.</small>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    <div class="form-group p-2">
                                                        <label for="exampleInputName2"></label>
                                                        <input name="emailSign" type="text" class="form-control"
                                                               id="exampleInputName2" placeholder="Your Email..." style="width: 50vh" required>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group p-2 pr-3">
                                                        <label for="exampleInputEmail2"></label>
                                                        <input name="passwordSign" type="password" class="form-control"
                                                               id="exampleInputEmail2" placeholder="Your Password..." style="width: 50vh" required>

                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <p style="font-size: 14px ;color: #fffacc">
                                                        <input type="checkbox" name="accept" value="ON" style=" width: 2vh; height: 2vh" />
                                                        I have read and accept the <a href="https://resy.com/privacy?date=2019-10-13&seats=2">Privacy Policy</a> and <a href="https://resy.com/terms?date=2019-10-13&seats=2">Terms of Service</a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p style="font-size: 14px ;color: #fffacc">
                                                        <input type="checkbox" name="letter" value="ON" style=" width: 2vh; height: 2vh" checked/>
                                                        Receive curated recommendations for where to dine, including hit lists, chef interviews, and more.
                                                    </p>
                                                </td>
                                            </tr>

                                        </table>                              
                                    </div>
                                </div>

                            </div>
                        </section>

                        <!--button Submit-->
                        <div class="modal-footer">
                            <input name="submitSign" type="submit" onclick="document.forms['signup'].submit();" value="Continue" class="btn btn-event-Modal" data-dismiss="modal" style="color: #53131e ; width: 100%; height: 100%;font-weight: bold"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </body>
</html>
