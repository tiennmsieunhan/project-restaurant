USE [Project]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[name] [nvarchar](50) NULL,
	[phone] [nvarchar](10) NULL,
	[email] [varchar](100) NOT NULL,
	[password] [nvarchar](20) NULL,
	[role] [nvarchar](5) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bookingTable]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bookingTable](
	[tableID] [int] NOT NULL,
	[chairsCount] [int] NULL,
	[date] [nchar](10) NOT NULL,
	[customerID] [int] NOT NULL,
 CONSTRAINT [PK_bookingTable] PRIMARY KEY CLUSTERED 
(
	[tableID] ASC,
	[date] ASC,
	[customerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[customers]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customers](
	[customerID] [int] NOT NULL,
	[customerName] [nvarchar](50) NULL,
	[email] [varchar](100) NULL,
	[phone] [varchar](10) NULL,
	[Note] [text] NULL,
 CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED 
(
	[customerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mealItems]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mealItems](
	[idMeal] [int] NOT NULL,
	[nameMeal] [nvarchar](10) NULL,
	[time] [varchar](8) NOT NULL,
	[idMealType] [int] NOT NULL,
	[tableID] [int] NOT NULL,
 CONSTRAINT [PK_mealItems_1] PRIMARY KEY CLUSTERED 
(
	[tableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mealType]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mealType](
	[idMealType] [int] NOT NULL,
	[nameMealType] [nvarchar](20) NULL,
 CONSTRAINT [PK_mealType] PRIMARY KEY CLUSTERED 
(
	[idMealType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[menuItems]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menuItems](
	[menuID] [int] NOT NULL,
	[itemID] [int] NOT NULL,
	[itemName] [nvarchar](100) NULL,
	[itemDescription] [nvarchar](1000) NULL,
	[prices] [float] NULL,
 CONSTRAINT [PK_menuItems_1] PRIMARY KEY CLUSTERED 
(
	[itemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[menus]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menus](
	[menuID] [int] NOT NULL,
	[menuVersion] [varchar](100) NULL,
 CONSTRAINT [PK_menus] PRIMARY KEY CLUSTERED 
(
	[menuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tableDay]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tableDay](
	[tableID] [int] NULL,
	[day] [nvarchar](8) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'Minh Hoang', N'0987654322', N'gamahoang1@gmail.com', N'hoang', N'user')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'Jackson Wang', N'0512346789', N'jacksonwang@gmail.com', N'jackson', N'user')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'Jonh', N'0512346789', N'jonh@gmail.com', N'jonh', N'user')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'Justin Bieber', N'0213456789', N'justinbieber@gmail.com', N'justin', N'user')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'Lily Colins', N'0312456789', N'lilycolins@gmail.com', N'lily', N'user')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'Min Rin', N'0123456789', N'minrin@gmail.com', N'min', N'admin')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'MinhCNN', N'0942739532', N'nhatminh140699@gmail.com', N'123456', N'user')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'Selena Gomes', N'0412356789', N'selenagomes@gmail.com', N'selena', N'user')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'anh', N'0123456781', N'tiennmsieunhan@gmail.com', N'tien', N'user')
INSERT [dbo].[Account] ([name], [phone], [email], [password], [role]) VALUES (N'Wang Yi Bo', N'0987654321', N'yibo@gmail.com', N'yibo', N'user')
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (13, 5, N'10/11/2019', 2)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (13, 1, N'10/11/2019', 5)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (14, 1, N'10/11/2019', 2)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (17, 1, N'25/04/2020', 7)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (18, 1, N'02/11/2019', 2)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (18, 4, N'17/11/2019', 2)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (25, 2, N'17/11/2019', 5)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (59, 3, N'03/11/2019', 4)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (61, 1, N'07/11/2019', 5)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (61, 5, N'10/11/2019', 2)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (61, 1, N'11/11/2019', 2)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (61, 1, N'12/11/2019', 5)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (61, 1, N'29/10/2019', 1)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (67, 5, N'04/11/2019', 6)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (67, 6, N'30/04/2020', 7)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (76, 1, N'29/10/2019', 1)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (76, 1, N'30/10/2019', 3)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (80, 4, N'06/11/2019', 6)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (93, 1, N'01/12/2019', 5)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (102, 1, N'30/11/2019', 3)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (103, 3, N'02/11/2019', 4)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (104, 7, N'11/11/2019', 5)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (105, 1, N'28/10/2020', 6)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (105, 2, N'29/10/2019', 2)
INSERT [dbo].[bookingTable] ([tableID], [chairsCount], [date], [customerID]) VALUES (105, 4, N'31/10/2019', 3)
INSERT [dbo].[customers] ([customerID], [customerName], [email], [phone], [Note]) VALUES (1, N'Wang Yi Bo', N'yibo@gmail.com', N'0987654321', N'me')
INSERT [dbo].[customers] ([customerID], [customerName], [email], [phone], [Note]) VALUES (2, N'Justin Bieber', N'justinbieber@gmail.com', N'0213456789', N'haha')
INSERT [dbo].[customers] ([customerID], [customerName], [email], [phone], [Note]) VALUES (3, N'Selena Gomes', N'selenagomes@gmail.com', N'0412356789', N'haha')
INSERT [dbo].[customers] ([customerID], [customerName], [email], [phone], [Note]) VALUES (4, N'Jonh', N'jonh@gmail.com', N'0512346789', N'')
INSERT [dbo].[customers] ([customerID], [customerName], [email], [phone], [Note]) VALUES (5, N'Lily Colins', N'lilycolins@gmail.com', N'0312456789', N'haha')
INSERT [dbo].[customers] ([customerID], [customerName], [email], [phone], [Note]) VALUES (6, N'Jackson Wang', N'jacksonwang@gmail.com', N'0512346788', N'fish')
INSERT [dbo].[customers] ([customerID], [customerName], [email], [phone], [Note]) VALUES (7, N'Minh Hoang', N'gamahoang1@gmail.com', N'0987654322', N'ok ok')
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:00PM', 1, 1)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:00PM', 2, 2)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:00PM', 3, 3)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:15PM', 1, 4)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:15PM', 2, 5)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:15PM', 3, 6)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:30PM', 1, 7)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:30PM', 2, 8)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:30PM', 3, 9)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:45PM', 1, 10)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:45PM', 2, 11)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'1:45PM', 3, 12)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:00AM', 1, 13)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:00AM', 2, 14)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:00AM', 3, 15)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:15AM', 1, 16)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:15AM', 2, 17)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:15AM', 3, 18)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:30AM', 1, 19)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:30AM', 2, 20)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:30AM', 3, 21)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:45AM', 1, 22)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:45AM', 2, 23)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'11:45AM', 3, 24)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:00PM', 1, 25)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:00PM', 2, 26)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:00PM', 3, 27)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:15PM', 1, 28)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:15PM', 2, 29)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:15PM', 3, 30)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:30PM', 1, 31)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:30PM', 2, 32)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:30PM', 3, 33)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:45PM', 1, 34)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:45PM', 2, 35)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'12:45PM', 3, 36)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:00PM', 1, 37)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:00PM', 2, 38)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:00PM', 3, 39)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:15PM', 1, 40)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:15PM', 2, 41)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:15PM', 3, 42)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:30PM', 1, 43)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:30PM', 2, 44)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:30PM', 3, 45)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:45PM', 1, 46)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:45PM', 2, 47)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'2:45PM', 3, 48)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:00PM', 1, 49)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:00PM', 2, 50)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:00PM', 3, 51)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:15PM', 1, 52)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:15PM', 2, 53)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:15PM', 3, 54)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:30PM', 1, 55)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:30PM', 2, 56)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:30PM', 3, 57)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:45PM', 1, 58)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:45PM', 2, 59)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (1, N'BRUNCH', N'3:45PM', 3, 60)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'5:00PM', 1, 61)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'5:00PM', 2, 62)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'5:15PM', 1, 63)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'5:15PM', 2, 64)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'5:30PM', 1, 65)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'5:30PM', 2, 66)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'5:45PM', 1, 67)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'5:45PM', 2, 68)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'6:15PM', 1, 69)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'6:15PM', 2, 70)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'6:30PM', 1, 71)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'6:30PM', 2, 72)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'6:45PM', 1, 73)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'6:45PM', 2, 74)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'7:00PM', 1, 75)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'7:00PM', 2, 76)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'7:15PM', 1, 77)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'7:15PM', 2, 78)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'7:30PM', 1, 79)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'7:30PM', 2, 80)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'7:45PM', 1, 81)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'7:45PM', 2, 82)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:00PM', 1, 83)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:00PM', 2, 84)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:15PM', 1, 85)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:15PM', 2, 86)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:15PM', 3, 87)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:30PM', 1, 88)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:30PM', 2, 89)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:30PM', 3, 90)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:45PM', 1, 91)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:45PM', 2, 92)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'8:45PM', 3, 93)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:00PM', 1, 94)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:00PM', 2, 95)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:00PM', 3, 96)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:15PM', 1, 97)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:15PM', 2, 98)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:15PM', 3, 99)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:30PM', 1, 100)
GO
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:30PM', 2, 101)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:30PM', 3, 102)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:45PM', 1, 103)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:45PM', 2, 104)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'9:45PM', 3, 105)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'6:00PM', 1, 106)
INSERT [dbo].[mealItems] ([idMeal], [nameMeal], [time], [idMealType], [tableID]) VALUES (2, N'DINNER', N'07:00PM', 3, 107)
INSERT [dbo].[mealType] ([idMealType], [nameMealType]) VALUES (1, N'DINING ROOM')
INSERT [dbo].[mealType] ([idMealType], [nameMealType]) VALUES (2, N'PATIO')
INSERT [dbo].[mealType] ([idMealType], [nameMealType]) VALUES (3, N'HIGH TOP')
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (1, 1, N'Savory Party Bread', N'round loaf sourdough bread, Monterey Jack cheese, butter, green onions,  poppy seeds', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (1, 2, N'Sweet & Spicy Jalapeno Poppers', N'jalapeno peppers, cheese, bacon strips, sugar,', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (1, 3, N'Cucumber-Stuffed Cherry  Tomatoes', N'cherry tomatoes, cheese, mayonnaise, cucumber, green onion, dill ', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (1, 4, N'Glazed Chicken Wings', N'chicken wings, barbecue sauce, honey', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (1, 5, N'Almond-Bacon Cheese Crostini', N'French bread baguette, bacon, Monterey Jack cheese, green onion, sliced almonds', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (1, 6, N'Taco Meatball Ring', N'cheddar cheese, ground beef, tomato, green onions, jalapeno peppers, sour cream    ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (1, 7, N'Best Deviled Eggs', N'milk, mayonnaise, parsley, dill, hard-boiled large eggs, garlic powder, paprika ', 5)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (1, 8, N'Homemade Guacamole', N'ripe avocados, garlic, onion, tomatoes, lime juice, cilantro  ', 5)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (2, 9, N'Chicken Croissant Sandwiches', N'chicken, apple, sunflower kernels,, green onions,  mandarin oranges, Lettuce leaves', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (2, 10, N'Veggie Steak Salad', N'sweet corn, beef flank steak,pepper, oilive ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (2, 11, N'Slow-Cooker Chicken Taco Salad', N'chili, ground cumin,paprika, oregano, chicken breasts  ', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (2, 12, N'Salmon Caesar Salad', N'salmon, fat-free creamy Caesar, garlic cloves, Parmesan cheese, almonds   ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (2, 13, N'Potato Soup', N'potatoes, onion, chicken,celery,bacon, cream,rubbed sage,  Parmesan cheese   ', 5)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (2, 14, N'Easy Pork Posole', N'onion, pork, chicken, tomatoes,corn tortillas, additional chopped onion, minced fresh cilantro and lime wedges ', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (2, 15, N'Seafood Cioppino', N'tomatoes, onions,clam juice,vegetable broth, red wine vinegar, Italian seasoning, crabmeat,haddock fillets, sugar   ', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (2, 16, N'Chunky Creamy Chicken Soup', N'skinless chicken breasts,  celery, onion, corn, cream of potato soup, half-and-half cream, peas,chicken broth, dill  ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (3, 17, N'Simple Shrimp Pad Thai', N'thick rice noodles, shrimp, eggs, dry roasted peanuts,cilantro leaves, sugar,sriracha chili sauce or hot pepper sauce  ', 11)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (3, 18, N'Beer-Steamed Mussels with Chorizo', N'Mexican beer, onion, Mexican chorizo, Hot sauce and crusty toasted bread,cilantro  ', 15)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (3, 19, N'Pork Chops with Celery ', N'cranberries, butter, unseasoned rice vinegar, bone-in pork rib chops, celery stalks,dry-roasted almonds  ', 13)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (3, 20, N'Pork Chops with Radishes and Charred Scallions', N'aniseed or fennel seeds,  bone-in pork chops , pepper,  lemon juice,  radishes', 13)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (3, 21, N'Red Snapper With Coconut-Clam Broth', N'red snapper, fennel seeds, fennel seeds, cilantro leaves,unsweetened shredded coconut   ', 12)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (3, 22, N'Brisket with Pomegranate-Walnut Sauce and Pistachio Gremolata', N'pomegranate juice, beef brisket,  black pepper, garlic, walnuts ', 15)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (3, 23, N'Roast Beef Tenderloin with Caesar Crust', N'beef tenderloin, Kosher salt, black pepper, Parmesan, breadcrumbs, Dijon mustard, lemon zest, Worcestershire sauce    ', 14)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (3, 24, N'Prosciutto-Stuffed Chicken with Mushroom Sauce', N'boneless chicken breasts, Kosher salt, ground pepper,  provolone cheese, basil leaves, unsalted butter, chicken broth,  red wine vinegar  ', 14)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (4, 25, N'Classic Sour Cherry Pie with Lattice Crust', N'all purpose flour, sugar, chilled unsalted butter, ice water   ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (4, 26, N'Strawberry, Pomegranate, and Rose Petal Mess', N'egg whites, granulated sugar,  powdered sugar, strawberries', 8)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (4, 27, N'Brownie Ice-Cream Sandwich', N'sweetened condensed milk, heavy cream, vanilla bean paste,  Oreos   ', 8)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (4, 28, N'Chocolate Grasshopper Ice Cream Tart', N' malted milk powder, unsweetened cocoa powder, Oreos, green mint chip ice cream, bittersweet or semisweet chocolate, corn syrup, heavy cream    ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (4, 29, N'Sweet Granita With Blackberries, Toasted Almonds, and Mint', N'rose water, sugar, blackberries, fresh lime juice, almonds,   mint leaves  ', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (4, 30, N'Black Ice Licorice Ice Cream', N' black licorice sticks, milk, heavy cream, egg yolks, granulated sugar, vanilla extract   ', 8)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (4, 31, N'Lemon Icebox Pie', N'graham crackers, sugar,  unsalted butter, salt,  lemon juice, egg yolks,  heavy cream   ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (4, 32, N'Alexander McCream Spiced Pumpkin Ice Cream', N'whole milk, heavy cream, egg yolks , granulated sugar,pumpkin, cinnamon, vanilla extract   ', 8)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (5, 33, N'Skillet-Charred Summer Beans with Miso Butter', N'unsalted butter, white miso, chile, ginger,  green beans, extra-virgin olive oil ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (5, 34, N'Pickled Rice Tabbouleh', N' cloves, extra-virgin olive oil, pumpkin seeds, lime juice, short-grain brown rice  ', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (5, 35, N'Cucumbers with Ajo Blanco Sauce', N'Persian cucumbers, red wine vinegar, almonds, extra-virgin olive oil  ', 5)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (5, 36, N'Spicy Smoked Gouda Twice-Baked Potatoes', N'potatoes, Gouda, mayonnaise, jalapeño chiles, green onion,  smoked paprika   ', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (5, 37, N'Roasted Root Vegetables', N'sweet potato, beets , carrots, parsnips, extra-virgin olive oil, maple syrup, cinnamon and nutmeg  ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (5, 38, N'Sweet Potato Fritters', N'sweet potatoes, all-purpose flour, red onion, egg, Vegetable oil  ', 6)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (5, 39, N'Crisp Hoi An Pancakes', N' rice flour, ground turmeric, kosher salt, shrimp, bean sprouts  ', 7)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (5, 40, N'Brussels Sprout Slaw', N'Brussels sprouts, olive oil, lemon juice, salt', 8)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (6, 41, N'Midnight Sparkler Cocktail', N'Lemon slice, crème de violette, orange juice, lemon juice, gin  ', 11)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (6, 42, N'Ginger, Lemon, and Sage', N'lemon, sprigs sage, sparkling wine, gin, liqueur, sugar ', 10)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (6, 43, N'Punch House Spritz', N'Cocchi Americano, Lini Lambrusco Rosato, grapefruit juice, soda water, grapefruit wheel  ', 11)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (6, 44, N'St-Germain Spritz', N'Prosecco, white wine,  St-Germain, Club soda,
Fresh lavender ', 10)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (6, 45, N'Sloe Gin Spritz', N'Prosecco, white wine, gin, club soda, Blueberries and a sprig of mint', 12)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (6, 46, N'Chandon Brut Mojito', N'mint leaves,  Simple Syrup, lime, rum, ice cubes,sparkling wine ', 10)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (6, 47, N'Derby Mint Julep', N'mint sprigs, sugar, mint leaves,  crushed ice, bourbon  ', 10)
INSERT [dbo].[menuItems] ([menuID], [itemID], [itemName], [itemDescription], [prices]) VALUES (6, 48, N'Mike''s Milk Punch', N'milk, bourbon, sugar, vanilla extract,  ice cubes  ', 9)
INSERT [dbo].[menus] ([menuID], [menuVersion]) VALUES (1, N'Appetizer')
INSERT [dbo].[menus] ([menuID], [menuVersion]) VALUES (2, N'Salad and Soup')
INSERT [dbo].[menus] ([menuID], [menuVersion]) VALUES (3, N'Main Meal')
INSERT [dbo].[menus] ([menuID], [menuVersion]) VALUES (4, N'Desserts')
INSERT [dbo].[menus] ([menuID], [menuVersion]) VALUES (5, N'Sides Meal')
INSERT [dbo].[menus] ([menuID], [menuVersion]) VALUES (6, N'Drinks')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (13, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (14, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (15, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (16, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (17, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (18, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (19, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (20, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (21, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (22, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (23, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (24, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (25, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (26, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (27, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (28, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (29, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (30, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (31, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (32, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (33, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (34, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (35, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (36, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (1, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (2, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (3, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (4, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (5, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (6, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (7, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (8, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (9, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (10, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (11, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (12, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (37, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (38, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (39, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (40, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (41, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (42, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (43, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (44, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (45, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (46, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (47, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (48, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (49, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (50, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (51, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (52, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (53, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (54, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (55, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (56, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (57, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (58, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (59, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (60, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (61, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (62, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (63, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (64, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (65, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (66, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (67, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (68, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (69, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (70, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (71, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (72, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (73, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (74, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (75, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (76, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (77, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (78, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (79, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (80, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (81, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (82, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (83, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (84, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (85, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (86, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (87, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (88, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (89, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (90, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (91, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (92, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (93, N'Sunday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (1, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (2, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (3, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (4, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (5, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (6, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (7, N'Saturday')
GO
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (8, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (9, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (10, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (11, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (12, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (13, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (14, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (15, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (16, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (17, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (18, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (19, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (20, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (21, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (22, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (23, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (24, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (25, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (26, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (27, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (28, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (29, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (30, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (31, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (32, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (33, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (34, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (35, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (36, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (37, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (38, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (39, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (40, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (41, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (42, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (43, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (44, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (45, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (46, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (47, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (48, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (49, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (50, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (51, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (52, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (53, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (54, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (55, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (56, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (57, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (58, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (59, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (60, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (61, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (62, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (63, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (64, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (65, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (66, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (67, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (68, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (69, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (70, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (71, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (72, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (73, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (74, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (75, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (76, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (77, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (78, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (79, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (80, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (81, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (82, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (83, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (84, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (85, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (86, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (87, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (88, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (89, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (90, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (91, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (92, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (93, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (94, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (95, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (96, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (97, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (98, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (99, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (100, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (101, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (102, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (103, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (104, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (105, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (61, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (62, N'Monday')
GO
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (63, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (64, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (65, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (66, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (67, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (68, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (69, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (70, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (71, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (72, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (73, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (74, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (75, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (76, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (77, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (78, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (79, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (80, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (81, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (82, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (83, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (84, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (85, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (86, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (87, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (88, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (89, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (90, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (91, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (92, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (93, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (94, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (95, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (96, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (97, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (98, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (99, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (100, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (101, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (102, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (103, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (104, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (105, N'Monday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (107, N'Saturday')
INSERT [dbo].[tableDay] ([tableID], [day]) VALUES (106, N'Sunday')
ALTER TABLE [dbo].[bookingTable]  WITH CHECK ADD  CONSTRAINT [FK_bookingTable_customers] FOREIGN KEY([customerID])
REFERENCES [dbo].[customers] ([customerID])
GO
ALTER TABLE [dbo].[bookingTable] CHECK CONSTRAINT [FK_bookingTable_customers]
GO
ALTER TABLE [dbo].[bookingTable]  WITH CHECK ADD  CONSTRAINT [FK_diningTables_mealItems] FOREIGN KEY([tableID])
REFERENCES [dbo].[mealItems] ([tableID])
GO
ALTER TABLE [dbo].[bookingTable] CHECK CONSTRAINT [FK_diningTables_mealItems]
GO
ALTER TABLE [dbo].[customers]  WITH CHECK ADD  CONSTRAINT [FK_customers_Account] FOREIGN KEY([email])
REFERENCES [dbo].[Account] ([email])
GO
ALTER TABLE [dbo].[customers] CHECK CONSTRAINT [FK_customers_Account]
GO
ALTER TABLE [dbo].[mealItems]  WITH CHECK ADD  CONSTRAINT [FK_meal_mealType1] FOREIGN KEY([idMealType])
REFERENCES [dbo].[mealType] ([idMealType])
GO
ALTER TABLE [dbo].[mealItems] CHECK CONSTRAINT [FK_meal_mealType1]
GO
ALTER TABLE [dbo].[menuItems]  WITH CHECK ADD  CONSTRAINT [FK_menuItems_menus] FOREIGN KEY([menuID])
REFERENCES [dbo].[menus] ([menuID])
GO
ALTER TABLE [dbo].[menuItems] CHECK CONSTRAINT [FK_menuItems_menus]
GO
ALTER TABLE [dbo].[tableDay]  WITH CHECK ADD  CONSTRAINT [FK_tableDay_mealItems] FOREIGN KEY([tableID])
REFERENCES [dbo].[mealItems] ([tableID])
GO
ALTER TABLE [dbo].[tableDay] CHECK CONSTRAINT [FK_tableDay_mealItems]
GO
/****** Object:  StoredProcedure [dbo].[GetMenuitems]    Script Date: 9/4/2021 5:59:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GetMenuitems]
@u int,
@v int
as
begin
 SELECT * FROM ( 
  SELECT *, ROW_NUMBER() OVER (ORDER BY itemID) as row 
  FROM menuItems a
 ) a WHERE a.row >= @u and a.row <= @v
end
GO
