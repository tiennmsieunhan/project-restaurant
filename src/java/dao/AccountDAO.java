/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;

/**
 *
 * @author Envy
 */
public class AccountDAO {

    //select
    public List<Account> select() throws Exception {
        List<Account> s = new ArrayList();
        String sql = "select * from Account";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String name = rs.getString("name");
            String phone = rs.getString("phone");
            String email = rs.getString("email");
            String pass = rs.getString("password");
            String role=rs.getString("role");
            s.add(new Account(name, phone, email, pass,role));
        }
        rs.close();
        conn.close();
        return s;
    }
    //select
    public Account getAccount(String ema) throws Exception {
        String sql = "select * from Account Where email = '"+ema+"'";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String name = rs.getString("name");
            String phone = rs.getString("phone");
            String email = rs.getString("email");
            String pass = rs.getString("password");
            String role=rs.getString("role");
            return new Account(name, phone, email, pass,role);
        }
        rs.close();
        conn.close();
        return null;
    }

    //insert new
    public void Insert(String name, String email) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;
        try {
            String s = "INSERT INTO Account(name,email,role)"
                    + "VALUES (?,?,?) ";

            pstmt = conn.prepareStatement(s);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.setString(3, "user");
            pstmt.executeUpdate();// dùng cho câu lệnh insert
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pstmt.close();
            conn.close();
        }
    }

    // sign up
    public void Insert(Account ac) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;
        try {
            String s = "INSERT INTO Account(name,phone,email,password,role)"
                    + "VALUES (?,?,?,?,?) ";

            pstmt = conn.prepareStatement(s);
            pstmt.setString(1, ac.getName());
            pstmt.setString(2, ac.getPhone());
            pstmt.setString(3, ac.getEmail());
            pstmt.setString(4, ac.getPassword());
            pstmt.setString(5, ac.getRole());
            pstmt.executeUpdate();// dùng cho câu lệnh insert
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pstmt.close();
            conn.close();
        }
    }
    // update Acc exits
    public void UpdateAcc(Account ac) throws Exception {
        Connection conn= new DBContext().getConnection();
        PreparedStatement pstmt = null;

        try {
            String upda = "UPDATE Account"
                    + " SET name=?,phone=?,password=?,role=?"
                    + " WHERE email =?";
            pstmt = conn.prepareStatement(upda);
            pstmt.setString(1, ac.getName() );
            pstmt.setString(2, ac.getPhone() );
            pstmt.setString(3, ac.getPassword() );
            pstmt.setString(4, ac.getRole() );
            pstmt.setString(5, ac.getEmail() );
            
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pstmt.close();
            conn.close();
        }
    }
}
