/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.MealIem;
import model.MealType;
import model.Table;
import model.tableDay;

/**
 *
 * @author Envy
 */
public class TableDAO implements Serializable{
    private List<Table> list;

    public TableDAO() {
    }

    public List<Table> getList() throws Exception {
        return select();
    }
    public List<String> getListnametype() throws Exception {
        List<String> lstr=new ArrayList();
        for(int i=0;i<select().size();i++)
        {
            lstr.add(this.select().get(i).getTypeName());
        }
        return lstr;
    }

    public void setList(List<Table> list) {
        this.list = list;
    }
    public List<MealType> getAlltype() throws Exception
    {
        return selectAlltype();
    }
   // select all Type
    private List<MealType> selectAlltype() throws Exception {
        List<MealType> s = new ArrayList();
        String sql = "select * from mealType  ";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int id=rs.getInt("idMealType");
            String name = rs.getString("nameMealType");
            s.add(new MealType(id, name.toUpperCase()));
        }
        rs.close();
        conn.close();
        return s;
    }
    
    
    
    
     //select
    public List<Table> select() throws Exception {
        List<Table> s = new ArrayList();
        String sql = "SELECT a.* FROM mealItems as a" ;
        
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int id = rs.getInt("idMeal");
            String name = rs.getString("nameMeal");
            String timem = rs.getString("time");
            String typen = this.getTypeName(rs.getInt("idMealType"));
            int tableid = rs.getInt("tableID");
            s.add(new Table(id, name, timem, typen, tableid));
        }
        rs.close();
        conn.close();
        return s;
    }
    public int searchTableIDMax() throws Exception
    {
        String sql = "SELECT MAX(tableID) as max FROM mealItems";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int typeid = rs.getInt("max");
            return typeid;
        }
        rs.close();
        conn.close();
        return -1;
    }
    //
     public String getTypeName(int search) throws Exception {
        String sql = "SELECT nameMealType FROM mealType WHERE idMealType = "+ search ;  
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String name = rs.getString("nameMealType");
            return name;
        }
        rs.close();
        conn.close();
        return null;
    }
     //
     public void addTable(int idMeal,String timem, int typeid) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;
        String nameMeal="BRUNCH";
        if(idMeal==1)
        {
            nameMeal="BRUNCH";
        }
        else if(idMeal==2)
        {
            nameMeal="DINNER";
        }
        try {
            String s = "INSERT INTO mealItems(idMeal,nameMeal,time,idMealType,tableID)"
                    + "VALUES (?,?,?,?,?) ";

            pstmt = conn.prepareStatement(s);
            pstmt.setInt(1, idMeal);
            pstmt.setString(2, nameMeal);
            pstmt.setString(3, timem);
            pstmt.setInt(4, typeid);
            pstmt.setInt(5, this.searchTableIDMax()+1);
            pstmt.executeUpdate();// dùng cho câu lệnh insert
        } catch (SQLException ex) {
            Logger.getLogger(TableDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
             conn.close();
            pstmt.close();
        }
    }
     public void UpdateTable(MealIem ta) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;
        try {
            String upda = "UPDATE mealItems"
                    + " SET idMeal=?,nameMeal=?,time=?,idMealType=?,tableID=?"
                    + " WHERE tableID =?";
            pstmt = conn.prepareStatement(upda);
            pstmt.setInt(1, ta.getId());
            pstmt.setString(2, ta.getName());
            pstmt.setString(3, ta.getTimem());
            pstmt.setInt(4, ta.getIdType());
            pstmt.setInt(5, ta.getTableid());
            pstmt.setInt(6, ta.getTableid());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TableDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conn.close();
            pstmt.close();
        }
    }
     public void DeleteTable(int tableid) throws Exception {
        Connection conn = new DBContext().getConnection();
        Statement stmt = null;
        try {
            String dele = "DELETE FROM mealItems"
                    + " WHERE tableID = "+tableid;
            stmt = conn.createStatement();
            stmt.execute(dele);         

        } catch (SQLException ex) {
            Logger.getLogger(TableDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conn.close();
            stmt.close();
        }
    }
     //////////////////
     public void addTableDay(String da) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;

        try {
            String s = "INSERT INTO tableDay(tableID,day)"
                    + "VALUES (?,?) ";

            pstmt = conn.prepareStatement(s);
            pstmt.setInt(1, this.searchTableIDMax() );
            pstmt.setString(2, da);
            pstmt.executeUpdate();// dùng cho câu lệnh insert
        } catch (SQLException ex) {
            Logger.getLogger(TableDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
             conn.close();
            pstmt.close();
        }
    }
     public void UpdateTableDay(tableDay da) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;
        try {
            String upda = "UPDATE tableDay"
                    + " SET tableID=?,day=?"
                    + " WHERE tableID =?";
            pstmt = conn.prepareStatement(upda);
            pstmt.setInt(1, da.getId() );
            pstmt.setString(2, da.getDay());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TableDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conn.close();
            pstmt.close();
        }
    }
     public void DeleteTableDay(int tableid) throws Exception {
        Connection conn = new DBContext().getConnection();
        Statement stmt = null;
        try {
            String dele = "DELETE FROM tableDay"
                    + " WHERE tableID = "+tableid;
            stmt = conn.createStatement();
            stmt.execute(dele);         

        } catch (SQLException ex) {
            Logger.getLogger(TableDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conn.close();
            stmt.close();
        }
    }
}
