/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.MealIem;

/**
 *
 * @author Envy
 */
public class BookingDAO {
    //select
    public Account getAccount(String email) throws Exception {
        String sql = "select * from Account Where email = '" + email + "'";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        System.out.println(sql);
        while (rs.next()) {
            String name = rs.getString("name");
            String phone = rs.getString("phone");
            String emaill = rs.getString("email");
            String password = rs.getString("password");
            String role = rs.getString("role");
            return new Account(name, phone, emaill, password, role);
        }
        rs.close();
        conn.close();
        return null;
    }

    public String getNameMeal(int tableID) throws Exception {
        String sql = "select nameMeal from mealItems Where tableID = " + tableID;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String nameMeal = rs.getString("nameMeal");
            return nameMeal;
        }
        rs.close();
        conn.close();
        return null;
    }

    public String getTimeBook(int tableID) throws Exception {
        String sql = "select time from mealItems Where tableID = " + tableID;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String timem = rs.getString("time");
            return timem;
        }
        rs.close();
        conn.close();
        return null;
    }

    public String searchNameType(int idMealType) throws Exception {
        String sql = "select nameMealType from mealType WHERE idMealType = " + idMealType;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String name = rs.getString("nameMealType");
            return name;
        }
        rs.close();
        conn.close();
        return null;
    }

    public int searchTypeID(int tableID) throws Exception {
        String sql = "select idMealType from mealItems WHERE tableID = " + tableID;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int typeid = rs.getInt("idMealType");
            return typeid;
        }
        rs.close();
        conn.close();
        return 0;
    }
    public int getCusID() throws Exception
    {
        String sql = "SELECT MAX(customerID) as max FROM customers";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int typeid = rs.getInt("max");
            return typeid;
        }
        rs.close();
        conn.close();
        return -1;
    }
    // insert 
    public void InsertCusFull(String name, String phone, String email, String note) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;
        try {
            String s = "INSERT INTO customers(customerID,customerName,email,phone,Note)"
                    + "VALUES (?,?,?,?,?) ";

            pstmt = conn.prepareStatement(s);
            pstmt.setInt(1, (this.getCusID()+1));
            pstmt.setString(2, name);
            pstmt.setString(3,email );
            pstmt.setString(4,phone);
            pstmt.setString(5,note);
            System.out.println(s);
            pstmt.executeUpdate();// dùng cho câu lệnh insert
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pstmt.close();
            conn.close();
        }
    }
     // insert 
    public void InsertCus(int cusid, String name, String phone, String email, String note) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;
        try {
            String s = "INSERT INTO customers(customerID,customerName,email,phone,Note)"
                    + "VALUES (?,?,?,?,?) ";

            pstmt = conn.prepareStatement(s);
            pstmt.setInt(1, cusid);
            pstmt.setString(2, name);
            pstmt.setString(3,email );
            pstmt.setString(4,phone);
            pstmt.setString(5,note);
            System.out.println(s);
            pstmt.executeUpdate();// dùng cho câu lệnh insert
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pstmt.close();
            conn.close();
        }
    }
    public int getcustomerID() throws Exception
    {
         String sql = "SELECT Max(customerID) as LastID FROM customers";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int cusid = rs.getInt("LastID");
            return cusid;
        }
        rs.close();
        conn.close();
        return 0;
    }
    public void InsertTabke(int tableid,int count, String day, int cusid) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;
        try {
            String s = "INSERT INTO bookingTable(tableID,chairsCount,date,customerID)"
                    + "VALUES (?,?,?,?) ";

            pstmt = conn.prepareStatement(s);
            pstmt.setInt(1, tableid);
            pstmt.setInt(2,count );
            pstmt.setString(3,day);
            pstmt.setInt(4,cusid);
            System.out.println(s);
            pstmt.executeUpdate();// dùng cho câu lệnh insert
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pstmt.close();
            conn.close();
        }
    }
    // delete booking table
    public void DeleteBookingTable(int tabid, int cusid) throws Exception {
        Connection conn = new DBContext().getConnection();
        Statement stmt = null;
        try {
            String dele = "DELETE FROM bookingTable"
                    + " WHERE tableID = " + tabid + " AND customerID = "+cusid;
            stmt = conn.createStatement();
            stmt.execute(dele);         

        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            stmt.close();
            conn.close();
        }
    }
    

}
