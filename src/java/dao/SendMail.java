/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.MailMessage;
import model.SMTPServer;

/**
 *
 * @author Envy
 */
public class SendMail {

    public Session getMailSession(SMTPServer mailServer, String from, String pass) {
        Properties props = new Properties();
        props.put("mail.smtp.host", mailServer.getServer());
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.socketFactory.port", mailServer.getPort());
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        // get the session object
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, pass);
            }
        });
        return session;               
    }
    public boolean send(MailMessage mm, Session session) throws Exception
    {
        Message message= new MimeMessage(session);
        message.setFrom(new InternetAddress(mm.getFrom()));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mm.getTo()));
        message.setSubject(mm.getSubject());
        message.setText(mm.getMessage());
        Transport.send(message);
        return true;
    }
}
