/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import model.BookingTable;
import model.MealIem;

/**
 *
 * @author Envy
 */
public class BookingTableDAO implements Serializable {
    int type;
    private int guest;
    private String day;
    int index;
    int sizelist;
    int size;

    public BookingTableDAO() {
        index = 0;
        day="";
        guest=1;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSizelist() throws Exception {
        return this.checksize(this.getListmeal());
    }

    public void setSizelist(int sizelist) {
        this.sizelist = sizelist;
    }

    public int getSize() throws Exception {
        return this.checksize(this.getListmeal2());
    }

    public void setSize(int size) {
        this.size = size;
    }

    private int checksize(List<MealIem> list) {
        int sizelist = list.size();
        if (sizelist % 6 == 0) {
            return sizelist / 6;
        } else {
            return (sizelist / 6) + 1;
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getGuest() {
        return guest;
    }

    public void setGuest(int guest) {
        this.guest = guest;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day.replaceAll("%2F", "/");
    }

    public List<BookingTable> getListbookingtable() throws Exception {
        return select();
    }
     public String getNametype() throws Exception {
        return this.searchtype();
    }

    private String searchtype() throws Exception {
        String sql = "select nameMealType from mealType WHERE idMealType = " + type;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String name = rs.getString("nameMealType");
            return name;
        }
        rs.close();
        conn.close();
        return null;
    }
    public String getDayTable() throws ParseException {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
        if (day.equals("")) {
            date = c.getTime();
        } else {
            
            date = formatter.parse(day);
        }
        c.setTime(date);
        return formatter.format(date) ;
    }
    //select

    public List<BookingTable> select() throws Exception {
        List<BookingTable> s = new ArrayList();
        String sql = "select * from bookingTable WHERE date = '" + this.getDayTable() + "' AND chairsCount = " + guest;
       
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int tableid = rs.getInt("tableID");
            int chairs = rs.getInt("chairsCount");
            String d = rs.getString("date");
            int cusid = rs.getInt("customerID");
            s.add(new BookingTable(tableid, chairs, d, cusid));
        }
        rs.close();
        conn.close();
        return s;
    }
 private String getDayOfWeek() throws ParseException {
        
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        if (day.equals("")) {
            date = c.getTime();
        } else {
            
            date = formatter.parse(day);
        }
        
        c.setTime(date);
        int dayofweek=c.get(Calendar.DAY_OF_WEEK);
        switch (dayofweek) {
            case 1:
                return "Sunday";
            case 7:
                return "Saturday";
            default:
                return "Monday";
        }
          
    }
     //select
    public List<MealIem> select(int search) throws Exception {
        List<MealIem> s = new ArrayList();
        String sql = "SELECT a.* FROM mealItems as a , tableDay as b WHERE a.tableID=b.tableID AND a.idMeal = " + search + " AND b.day= '" + this.getDayOfWeek() + "'  ORDER BY a.time";
        System.out.println(sql+"   mai tien   ");
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int id = rs.getInt("idMeal");
            String name = rs.getString("nameMeal");
            String timem = rs.getString("time");
            int idtype = rs.getInt("idMealType");
            int tableid = rs.getInt("tableID");
            s.add(new MealIem(id, name, timem, idtype, tableid));
        }
        rs.close();
        conn.close();
        return s;
    }
    //
    public List<MealIem> getListmeal() throws Exception {
        List<MealIem> listmeal =this.select(1);
        List<BookingTable> listtable = this.select();
        if(listtable.size()<=0) 
        {
            return listmeal;
        }
        for (int i = 0; i < listmeal.size(); i++) {
            for (int j = 0; j < listtable.size(); j++) {
                if (listmeal.get(i).getTableid() == listtable.get(j).getTableid()) {
                     listmeal.remove(listmeal.get(i));
                }
            }
        }
        return listmeal;
    }

    //
    public List<MealIem> getListmeal2() throws Exception {
        List<MealIem> listmeal = this.select(2);
        List<BookingTable> listtable = this.select();
        Collections.sort(listmeal, new Comparator<MealIem>() {
            @Override
            public int compare(MealIem o1, MealIem o2) {
                return o1.getTimem().compareTo(o2.getTimem());
            }

        });
        if(listtable.size()<=0)
        {
            return listmeal;
        }
        
        for (int i = 0; i < listmeal.size(); i++) {
            for (int j = 0; j < listtable.size(); j++) {
                if (listmeal.get(i).getTableid() == listtable.get(j).getTableid()) {
                    listmeal.remove(listmeal.get(i));
                }
            }
        }
         Collections.sort(listmeal, new Comparator<MealIem>() {
            @Override
            public int compare(MealIem o1, MealIem o2) {
                return o1.getTimem().compareTo(o2.getTimem());
            }

        });
        return listmeal;
    }
    //
    public List<MealIem> getListsmeal1() throws Exception {
        List<MealIem> list = this.getListmeal();
        ArrayList<MealIem> list1 = new ArrayList();
        ArrayList<MealIem> list2 = new ArrayList();
        for (MealIem a : list) {
            if (a.getTimem().contains("AM")) {
                list1.add(a);
            } else {
                list2.add(a);
            }
        }
        Collections.sort(list1, new Comparator<MealIem>() {
            @Override
            public int compare(MealIem o1, MealIem o2) {
                return o1.getTimem().compareTo(o2.getTimem());
            }

        });
        Collections.sort(list2, new Comparator<MealIem>() {
            @Override
            public int compare(MealIem o1, MealIem o2) {
                return o1.getTimem().compareTo(o2.getTimem());
            }

        });
        for(MealIem e: list2)
        {
            list1.add(e);
        }
        return list1;
    }
   
}
