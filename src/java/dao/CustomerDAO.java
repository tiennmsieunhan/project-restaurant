/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Envy
 */
public class CustomerDAO {

    //select
    public Customer select(int cusID) throws Exception {
        String sql = "select * from customers WHERE customerID = " + cusID;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int cusid = rs.getInt("customerID");
            String namecus = rs.getString("customerName");
            String email = rs.getString("email");
            String phone = rs.getString("phone");
            String note = rs.getString("Note");
            return new Customer(cusid, namecus, phone, email, note);
        }
        rs.close();
        conn.close();
        return null;
    }

    public List<Customer> selectAll() throws Exception {
        List<Customer> s = new ArrayList();
        String sql = "select * from customers";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int cusid = rs.getInt("customerID");
            String namecus = rs.getString("customerName");
            String email = rs.getString("email");
            String phone = rs.getString("phone");
            String note = rs.getString("Note");
            s.add(new Customer(cusid, namecus, phone, email, note));
        }
        rs.close();
        conn.close();
        return s;
    }
    //
    public int getCusid(String email) throws Exception
    {
        String sql = "select customerID from customers WHERE email = '" +email+"'" ;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int cusid = rs.getInt("customerID");
            return cusid;
        }
        rs.close();
        conn.close();
        return 0;
    }
    // update Acc exits

    public void UpdateAcc(Customer cus) throws Exception {
        Connection conn = new DBContext().getConnection();
        PreparedStatement pstmt = null;

        try {
            String upda = "UPDATE customers"
                    + " SET customerName=?,phone=?,Note=?"
                    + " WHERE customerID =?";
            pstmt = conn.prepareStatement(upda);
            pstmt.setString(1, cus.getCustomerName());
            pstmt.setString(2, cus.getPhone());
            pstmt.setString(3, cus.getNote());
            pstmt.setInt(4, cus.getCustomerID());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pstmt.close();
            conn.close();
        }
    }
}
