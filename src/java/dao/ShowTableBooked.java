/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import model.BookingTable;
import model.Customer;
import model.MealIem;

/**
 *
 * @author Envy
 */
public class ShowTableBooked implements Serializable{
    private String day;
    private int tabid;
    public ShowTableBooked() {
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day.replaceAll("%2F", "/");;
    }

    public int getTabid() {
        return tabid;
    }

    public void setTabid(int tabid) {
        this.tabid = tabid;
    }
    
    public String getDayTable() throws ParseException {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
        if (day.equals("")) {
            date = c.getTime();
        } else {
            
            date = formatter.parse(day);
        }
        c.setTime(date);
        return formatter.format(date) ;
    }
    public List<BookingTable> getListshow() throws Exception
    {
        return select();
    }
   // select table booking
    public List<BookingTable> select() throws Exception {
        List<BookingTable> s = new ArrayList();
        String sql = "select * from bookingTable WHERE date = '" + this.getDayTable() + "'";
       
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int tableid = rs.getInt("tableID");
            int chairs = rs.getInt("chairsCount");
            String d = rs.getString("date");
            int cusid = rs.getInt("customerID");
            s.add(new BookingTable(tableid, chairs, d, cusid));
        }
        rs.close();
        conn.close();
        return s;
    }
    //
    public String getNamemeal() throws Exception
    {
        return selectMealitem().getName();
    }
     public String getTimemeal() throws Exception
    {
        return selectMealitem().getTimem();
    }
     public String getNametype() throws Exception
     {
         return this.searchNameType(selectMealitem().getIdType());
     }
    public MealIem selectMealitem() throws Exception {
        String sql = "SELECT * FROM mealItems WHERE tableID = " +tabid;
        
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int id = rs.getInt("idMeal");
            String name = rs.getString("nameMeal");
            String timem = rs.getString("time");
            int idtype = rs.getInt("idMealType");
            int tableid = rs.getInt("tableID");
            return new MealIem(id, name, timem, idtype, tableid);
        }
        rs.close();
        conn.close();
        return null;
    }
    //
    public String searchNameType(int idMealType) throws Exception {
        String sql = "select nameMealType from mealType WHERE idMealType = " + idMealType;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String name = rs.getString("nameMealType");
            return name;
        }
        rs.close();
        conn.close();
        return null;
    }
    
    
}
