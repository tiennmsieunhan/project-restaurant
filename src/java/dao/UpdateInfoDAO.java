/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import model.BookingTable;
import model.Customer;
import model.MealIem;

/**
 *
 * @author Envy
 */
public class UpdateInfoDAO implements Serializable {

    private String email;
    private int index;

    public UpdateInfoDAO() {
        index = 0;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTabid() throws Exception {
        return this.gettableID().get(index);
    }

    public int getTableSize() throws Exception {
        return this.gettableID().size();
    }

    public MealIem getMealitem() throws Exception {
        return this.selectMealitem();
    }

    public String getNameType() throws Exception {
        return this.searchNameType(this.selectMealitem().getIdType());
    }

    public BookingTable getBookingtable() throws Exception {
        return this.selectBook();
    }

    public int getCustomerid() throws Exception {
        return this.getCusid();
    }

    public boolean getChecktime() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String da = simpleDateFormat.format(new Date());
        Date date =  new Date();
        date=formatter.parse(this.getBookingtable().getDay());
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
         java.sql.Date date1 = java.sql.Date.valueOf(da);
        java.sql.Date date2 = java.sql.Date.valueOf(simpleDateFormat.format(date));
        c1.setTime(date1);
        c2.setTime(date2);
        long noDay = (c2.getTime().getTime() - c1.getTime().getTime()) / (24 * 3600 * 1000);
        System.out.println(noDay);
        if(noDay<1) return false;
        else return true;
    }

    public String getNotetable() throws Exception {

        String sql = "Select Note  from customers as a , bookingTable as b WHERE a.customerID=b.customerID and b.tableID = " + this.getTabid();

        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String note = rs.getString("Note");
            return note;
        }
        rs.close();
        conn.close();
        return null;
    }

    // select table booking
    public BookingTable selectBook() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String da = simpleDateFormat.format(new Date());
        String sql = "select * from bookingTable as b WHERE b.customerID = " + this.getCusid() + " AND b.tableID = " + this.getTabid() + " and CONVERT(DATE,b.date,103) >= '" + da + "'";
       
        System.out.println(sql);
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int tableid = rs.getInt("tableID");
            int chairs = rs.getInt("chairsCount");
            String d = rs.getString("date");
            int cusid = rs.getInt("customerID");
            return new BookingTable(tableid, chairs, d, cusid);
        }
        rs.close();
        conn.close();
        return null;
    }

    public int getCusid() throws Exception {
        String sql = "select customerID from customers WHERE email = '" + email + "'";
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int cusid = rs.getInt("customerID");
            return cusid;
        }
        rs.close();
        conn.close();
        return 0;
    }

    public Customer getInfoCustomer() throws Exception {
        String sql = "select * from customers WHERE customerID = " + this.getCusid();
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int cusid = rs.getInt("customerID");
            String namecus = rs.getString("customerName");
            String email = rs.getString("email");
            String phone = rs.getString("phone");
            String note = rs.getString("Note");
            return new Customer(cusid, namecus, phone, email, note);
        }
        rs.close();
        conn.close();
        return null;
    }

    //
    public List<Integer> gettableID() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String da = simpleDateFormat.format(new Date());
        List<Integer> listtableid = new ArrayList();
        String sql = "select tableID from bookingTable as b WHERE b.customerID = " + this.getCusid() + " and CONVERT(DATE,b.date,103) >= '" + da + "'";
        System.out.println(sql);
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int cusid = rs.getInt("tableID");
            listtableid.add(cusid);
        }
        rs.close();
        conn.close();
        return listtableid;
    }

    //
    public String searchNameType(int idMealType) throws Exception {
        String sql = "select nameMealType from mealType WHERE idMealType = " + idMealType;
        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            String name = rs.getString("nameMealType");
            return name;
        }
        rs.close();
        conn.close();
        return null;
    }

    //
    public MealIem selectMealitem() throws Exception {
        String sql = "SELECT * FROM mealItems WHERE tableID = " + this.getTabid();

        Connection conn = new DBContext().getConnection();
        ResultSet rs = conn.prepareStatement(sql).executeQuery();
        while (rs.next()) {
            int id = rs.getInt("idMeal");
            String name = rs.getString("nameMeal");
            String timem = rs.getString("time");
            int idtype = rs.getInt("idMealType");
            int tableid = rs.getInt("tableID");
            return new MealIem(id, name, timem, idtype, tableid);
        }
        rs.close();
        conn.close();
        return null;
    }

}
