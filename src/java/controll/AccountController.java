/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controll;

import dao.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;

/**
 *
 * @author Envy
 */
public class AccountController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private boolean checkAcc(String email) throws Exception {
        AccountDAO da = new AccountDAO();
        List<Account> list = da.select();
        for (int i = 0; i < list.size(); i++) {
            if (email.equals(list.get(i).getEmail())) {
                return false;
            }
        }
        return true;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF- 8");
        String str= request.getParameter("submitSign");
        try {
            AccountDAO dao = new AccountDAO();
            
            if (request.getParameter("submitFind") != null) {
                String namef = request.getParameter("nameFind");
                String emailf = request.getParameter("emailFind");
                dao.Insert(namef, emailf);
                response.sendRedirect("index.jsp");
            }

            if (request.getParameter("submitSign") != null) {
//                if(request.getParameter("nameSign").equals("")|| request.getParameter("phoneSign").equals("")|| request.getParameter("emailSign").equals("")|| request.getParameter("passwordSign").equals(""))
//                {
//                    return;
//                }
                String names = request.getParameter("nameSign");
                String phones = request.getParameter("phoneSign");
                String emails = request.getParameter("emailSign");
                String passwords = request.getParameter("passwordSign");
                Account ac = new Account(names, phones, emails, passwords, "user");
                
                if(this.checkAcc(emails)){
                dao.Insert(ac);
                }
                else
                {
                    dao.UpdateAcc(ac);
                }
                 RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
            rd.forward(request, response); 
              //  response.sendRedirect("index.jsp");
            }

        } catch (Exception e) {
            response.getWriter().println(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
