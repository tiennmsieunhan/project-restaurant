/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controll;

import dao.BookingDAO;
import dao.CustomerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Customer;

/**
 *
 * @author Envy
 */
public class BookedCustomerController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private Customer checkCustomerID(String email) throws Exception {
        CustomerDAO d = new CustomerDAO();
        List<Customer> list = d.selectAll();
        for (Customer c : list) {
            if (c.getEmail().compareTo(email) == 0) {
                return c;
            }
        }
        return null;
    }
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            BookingDAO dao = new BookingDAO();
            if (request.getParameter("booking") != null) {
                String name = request.getParameter("nameBook");
                String phone = request.getParameter("phoneBook");
                String email = request.getParameter("emailBook");
                String note = request.getParameter("noteBook");
                int cusid;
                if (this.checkCustomerID(email) != null) {
                    dao.InsertCus(this.checkCustomerID(email).getCustomerID(), name, phone, this.checkCustomerID(email).getEmail(), note);
                    cusid=this.checkCustomerID(email).getCustomerID();
                } else {
                    dao.InsertCusFull(name, phone, email, note);
                    cusid = dao.getcustomerID();
                }
                System.out.println(cusid);
                String str = request.getParameter("booking");
                String[] s = str.split(",");
                int tableID = Integer.valueOf(s[0]);
                String day = s[1];
                int guest = Integer.parseInt(s[2]);
                dao.InsertTabke(tableID, guest, day, cusid);
                String sus = "Booking Successfully!!!";
                request.setAttribute("sus", sus);

            }
            response.sendRedirect("/res/user/reservation.jsp");
            
        } catch (Exception e) {
            response.getWriter().println(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
