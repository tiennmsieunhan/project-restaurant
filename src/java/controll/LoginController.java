/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controll;

import dao.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author Envy
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private Account checkAcc(String user, String pass) {
        AccountDAO da = new AccountDAO();
        try {
            List<Account> list = da.select();
            for (int i = 0; i < list.size(); i++) {
                if (user.equals(list.get(i).getEmail()) && pass.equals(list.get(i).getPassword())) {
                    String role = list.get(i).getRole();
                    return list.get(i);
                }
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            PrintWriter out = response.getWriter();
            //do valication
            //   if (request.getParameter("submitLogin") != null) {
            String user = request.getParameter("emailLogin");
            String pass = request.getParameter("passLogin");
            System.out.println(user);
            HttpSession session = request.getSession(true);

            if (this.checkAcc(user, pass) != null) {
                Account ac = this.checkAcc(user, pass);
                session.setAttribute("login", ac);
                session.setMaxInactiveInterval(15 * 60);
                if (ac.getRole().equals("admin")) {
                    response.sendRedirect("admin/reservation.jsp");

                } else {
                    response.sendRedirect("user/reservation.jsp");
                }

            } else {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('User or password incorrect');");
                out.println("location='reservation.jsp';");
                out.println("</script>");
            }
//            else {
//                    session.setAttribute("error", "We couldn't validate your login information."
//                            + " Please check that your email and password are correct.");
//                    response.sendRedirect("reservation.jsp");
//
//                }
            //   }

        } catch (Exception e) {
            response.getWriter().println(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
