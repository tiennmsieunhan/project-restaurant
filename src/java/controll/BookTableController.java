/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controll;

import dao.BookingDAO;
import dao.CustomerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Customer;

/**
 *
 * @author Envy
 */
public class BookTableController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private String getDayOfWeek(int value) {
        String day = "";
        switch (value) {
            case 1:
                day = "Sunday";
                break;
            case 2:
                day = "Monday";
                break;
            case 3:
                day = "Tuesday";
                break;
            case 4:
                day = "Wednesday";
                break;
            case 5:
                day = "Thursday";
                break;
            case 6:
                day = "Friday";
                break;
            case 7:
                day = "Saturday";
                break;
        }
        return day;
    }

    private String getMonthofYearBook(String day) {
        String[] s = day.split("/");
        int check = Integer.parseInt(s[1]);
        switch (check) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
        }
        return null;
    }

    private String getDayofWeekBook(String day) throws ParseException {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date d = formatter.parse(day);
        c.setTime(d);
        int dayofweek = c.get(Calendar.DAY_OF_WEEK);

        return this.getDayOfWeek(dayofweek);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            PrintWriter out = response.getWriter();
            if (request.getParameter("day").equals("")) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Please choise date for reservation !');");
                out.println("location='/res/user/reservation.jsp';");
                out.println("</script>");
                
            }
            
            String tableID = request.getParameter("tablebook");
            String emailac = request.getParameter("email");
            String day = request.getParameter("day");
            day = day.replaceAll("%2F", "/");
            int guest = Integer.parseInt(request.getParameter("guest"));
            BookingDAO dao = new BookingDAO();
            Account acc = dao.getAccount(emailac);
            String[] str = day.split("/");
            String cal = this.getDayofWeekBook(day) + ", " + this.getMonthofYearBook(day) + " " + str[0] + ", " + str[2] + " " + dao.getTimeBook(Integer.valueOf(tableID));
            //
            request.setAttribute("cal", cal);
            int typeid = dao.searchTypeID(Integer.valueOf(tableID));
            String guestC = "Party of " + guest + ", " + dao.getNameMeal(Integer.valueOf(tableID)) + " " + dao.searchNameType(typeid);
            //
            request.setAttribute("infoguest", guestC);
            //
            request.setAttribute("acc", acc);
            // 
            String send = tableID + "," + day + "," + guest;
            request.setAttribute("send", send);
            RequestDispatcher rd = request.getRequestDispatcher("booktable.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
            response.getWriter().println(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
