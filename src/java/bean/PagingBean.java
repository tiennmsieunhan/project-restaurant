/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import context.DBContext;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.MenuItem;

/**
 *
 * @author Envy
 */
public class PagingBean implements Serializable{
    private int size;// so dong trong 1 trang
    private int page;
    private int pages;

    public PagingBean() {
        size=8;
        page=1;
    }
    // list of product from i to j
    public List<MenuItem> getMenuitems() throws Exception
    {
        int i= (page-1)*size+1;
        int j= page*size;
        
        Connection conn= new DBContext().getConnection();
        CallableStatement cs=conn.prepareCall("{call GetMenuitems (?,?)}");
        cs.setInt(1, i);
        cs.setInt(2, j);
        ResultSet rs= cs.executeQuery();
        List<MenuItem> p= new ArrayList();
        while(rs.next())
        {
            int menuID=rs.getInt("menuID");
            int itemID=rs.getInt("itemID");
            String name= rs.getString("itemName");
            String des=rs.getString("itemDescription");
            float price=rs.getFloat("prices");
            p.add(new MenuItem(menuID, itemID, name, des, price));
                    
        }
        cs.close();
        conn.close();
        return p;
    }
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() throws Exception {
        String sql="select count(*) from menuItems";
        ResultSet rs=new DBContext().getConnection().prepareStatement(sql).executeQuery();
        int rows=0;
        if(rs.next())
        {
            rows=(int) rs.getInt(1);
        }
        pages=(rows<size)?1:rows/size;
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
    
    
}
