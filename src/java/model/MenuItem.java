/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Envy
 */
public class MenuItem {
    private int menuID;
    private int itemID;
    private String itemName;
    private String itemDes;
    private float price;

    public MenuItem() {
    }

    public MenuItem(int menuID, int itemID, String itemName, String itemDes, float price) {
        this.menuID = menuID;
        this.itemID = itemID;
        this.itemName = itemName;
        this.itemDes = itemDes;
        this.price = price;
    }

    public int getMenuID() {
        return menuID;
    }

    public void setMenuID(int menuID) {
        this.menuID = menuID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDes() {
        return itemDes;
    }

    public void setItemDes(String itemDes) {
        this.itemDes = itemDes;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    
}
