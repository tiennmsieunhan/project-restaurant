/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


/**
 *
 * @author Envy
 */
public class BookingTable {
    private int tableid;
    private int guest;
    private String day;
    private int customerid;

    public BookingTable() {
    }

    public BookingTable(int tableid, int guest, String day, int customerid) {
        this.tableid = tableid;
        this.guest = guest;
        this.day = day;
        this.customerid = customerid;
    }

    public int getTableid() {
        return tableid;
    }

    public void setTableid(int tableid) {
        this.tableid = tableid;
    }

    public int getGuest() {
        return guest;
    }

    public void setGuest(int guest) {
        this.guest = guest;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    

    public int getCustomerid() {
        return customerid;
    }

    public void setCustomerid(int customerid) {
        this.customerid = customerid;
    }
    
}
