/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Envy
 */
public class Table {
    int id;
    String name;
    String timem;
    String typeName;
    int tableid;
    public Table() {
    }

    public Table(int id, String name, String timem, String idType, int tableid) {
        this.id = id;
        this.name = name;
        this.timem = timem;
        this.typeName= idType;
        this.tableid= tableid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimem() {
        return timem;
    }

    public void setTimem(String timem) {
        this.timem = timem;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

   

    public int getTableid() {
        return tableid;
    }

    public void setTableid(int tableid) {
        this.tableid = tableid;
    }
    
}
